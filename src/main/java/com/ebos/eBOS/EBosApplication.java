package com.ebos.eBOS;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.ebos.eBOS.data.entity.MinIncrementRule;
import com.ebos.eBOS.service.AuctionService;
import com.ebos.eBOS.service.MinIncrementRuleService;
import com.ebos.eBOS.service.UserService;
import com.ebos.eBOS.util.MinIncrementUtils;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.TimeZone;

@SpringBootApplication
public class EBosApplication {


    public static void main(String[] args) {
        SpringApplication.run(EBosApplication.class, args);
    }


    @Component
    public class CommandLineAppStartupRunner implements CommandLineRunner {
        private MinIncrementRuleService minIncrementRuleService;
        private AuctionService auctionService;
        private UserService userService;

        @Autowired
        public void setAuctionService(AuctionService auctionService) {
            this.auctionService = auctionService;
        }

        @Autowired
        public void setUserService(UserService userService) {
            this.userService = userService;
        }

        @Autowired
        public void setMinIncrementRuleService(MinIncrementRuleService minIncrementRuleService) {
            this.minIncrementRuleService = minIncrementRuleService;
        }

        @Override
        public void run(String... args) throws IOException {
            initFBDatabaseApp();

            List<MinIncrementRule> minIncrementRules = minIncrementRuleService.getAll();
            MinIncrementUtils.setUpRules(minIncrementRules);

//            auctionService.getActiveAuctions().forEach(auction -> {
//                auction.setPlannedEndTime(auction.getStartTime().plusDays(auction.getDurationInDay()));
//                auctionService.save(auction);
//                System.out.println(auction.getId());
//
//            });
//
//            auctionService.getEndedAuctions().forEach(auction -> {
//                auction.setPlannedEndTime(auction.getStartTime().plusDays(auction.getDurationInDay()));
//                auction.setActualEndTime(auction.getPlannedEndTime());
//                auctionService.save(auction);
//                System.out.println(auction.getId());
//            }
//            );

//            userService.getAllUsers().forEach(user -> {
//                user.setStatus(AppConstants.USER_STT_ACTIVE);
//                userService.save(user);
//            });
        }
    }

    /**
     * Initialize Firebase's Database
     *
     * @throws IOException
     */
    public static void initFBDatabaseApp() throws IOException {
        FileInputStream serviceAccount = new FileInputStream("ebos-359d2-firebase-adminsdk-u0oda-41117e86d8.json");

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl(AppConstants.FIREBASE_DATABASE_URL)
                .build();

        FirebaseApp.initializeApp(options);

    }

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
    }

    @Bean
    public Cloudinary cloudinary() {
        return new Cloudinary(ObjectUtils.asMap(
                "cloud_name", AppConstants.CLOUDINARY_CLOUD_NAME,
                "api_key", AppConstants.CLOUDINARY_API_KEY,
                "api_secret", AppConstants.CLOUDINARY_API_SECRET));
    }

}
