package com.ebos.eBOS;

public class AppConstants {
    // server config
    public static final String DEV_EBOS_ENDPOINT = "http://localhost:8080";
    public static final String PROD_EBOS_ENDPOINT = "https://ebos-backend.herokuapp.com";
    public static final String FILE_DIR = "/files";

    // media's type
    public static final String MEDIA_TYPE_VIDEO = "VIDEO";
    public static final String MEDIA_TYPE_IMAGE = "IMAGE";

    // auction's status
    public static final String AUCTION_STT_PENDING_FOR_ACCEPTING = "PENDING_FOR_ACCEPTING";
    public static final String AUCTION_STT_ACTIVE = "ACTIVE";
    public static final String AUCTION_STT_REJECTED = "REJECTED";
    public static final String AUCTION_STT_ENDED = "ENDED";
    public static final String AUCTION_STT_CANCELLED_BY_ADMIN = "CANCELLED_BY_ADMIN";
    public static final String AUCTION_STT_CANCELLED_BY_SELLER = "CANCELLED_BY_SELLER";

    // user's status
    public static final String USER_STT_ACTIVE = "ACTIVE";
    public static final String USER_STT_RESTRICTED = "RESTRICTED";

    // reject auction reason
    public static final String REJECT_REASON_DUPLICATED_ITEM = "DUPLICATED_ITEM";
    public static final String REJECT_REASON_INAPPROPRIATE_IMAGES = "INAPPROPRIATE_IMAGES";
    public static final String REJECT_REASON_INAPPROPRIATE_INFO = "INAPPROPRIATE_INFO";
    public static final String REJECT_REASON_PROHIBITED_RESTRICTED_ITEM = "PROHIBITED_RESTRICTED_ITEM";

    // firebase's database paths
    public static final String FIREBASE_DATABASE_URL = "https://ebos-359d2.firebaseio.com";
    public static final String FIREBASE_AUCTIONS_PATH = "ebos/auctions";

    // cloudinary config
    public static final String CLOUDINARY_CLOUD_NAME = "dj7cc28rx";
    public static final String CLOUDINARY_API_KEY = "549582248264629";
    public static final String CLOUDINARY_API_SECRET = "2XmPw0wcnMUqBAqb7Rw7_k5Uin4";

    // sort type
    public static final String SORT_CURRENT_PRICE = "CURRENT_PRICE";
    public static final String SORT_NUM_OF_BID = "NUM_OF_BID";
    public static final String SORT_TIME = "TIME";
    public static final String SORT_ENDED_TIME = "ENDED_TIME";
    public static final String SORT_TIME_LEFT = "TIME_LEFT";

    // sort direction
    public static final String DIR_ASC = "ASC";
    public static final String DIR_DESC = "DESC";
    public static final String DIR_NEWLY_LISTED = "NEWLY_LISTED";
    public static final String DIR_ENDING_SOON = "ENDING_SOON";

    // Notification
    public static final String N_TITLE_DEFAULT = "Thông báo từ eBOS";
    public static final String N_DATA_AUCTION_ID = "NOTIFICATION_DATA_AUCTION_ID";

    // Job map data key
    public static final String JMD_NOTIFICATION_TITLE = "NOTIFICATION_TITLE";
    public static final String JMD_NOTIFICATION_BODY = "NOTIFICATION_BODY";
    public static final String JMD_NOTIFICATION_USER_ID = "NOTIFICATION_USER_ID";
    public static final String JMD_NOTIFICATION_DATA_AUCTION_ID = "NOTIFICATION_DATA_AUCTION_ID";
    public static final String JMD_NOTIFICATION_DATA_SCREEN = "NOTIFICATION_DATA_SCREEN";
    public static final String JMD_AUCTION_ID = "AUCTION_ID";
    public static final String JMD_AUCTION_STATUS = "AUCTION_STATUS";

    // Job key
    public static final String JOB_KEY_NOTIFICATION_WHEN_AUCTION_ENDED = "NOTIFICATION_USER_{0}_WHEN_AUCTION_{1}_ENDED";
    public static final String JOB_KEY_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY = "NOTIFICATION_USER_{0}_WHEN_AUCTION_{1}_ENDS_IN_ONE_DAY";
    public static final String JOB_KEY_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR = "NOTIFICATION_USER_{0}_WHEN_AUCTION_{1}_ENDS_IN_ONE_HOUR";
    public static final String JOB_KEY_UPDATE_AUCTION_STATUS_WHEN_IT_ENDED = "UPDATE_AUCTION_{0}_STATUS_WHEN_IT_ENDED";


    // Job group
    public static final String JOB_GROUP_NOTIFICATION_WHEN_AUCTION_ENDED = "NOTIFICATION_WHEN_AUCTION_ENDED";
    public static final String JOB_GROUP_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY = "NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY";
    public static final String JOB_GROUP_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR = "NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR";
    public static final String JOB_GROUP_UPDATE_AUCTION_STATUS_WHEN_IT_ENDED = "UPDATE_AUCTION_STATUS_WHEN_IT_ENDED";

    // Trigger group
    public static final String TRIGGER_GROUP_NOTIFICATION_WHEN_AUCTION_ENDED = "NOTIFICATION_WHEN_AUCTION_ENDED";
    public static final String TRIGGER_GROUP_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY = "NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY";
    public static final String TRIGGER_GROUP_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR = "NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR";
    public static final String TRIGGER_GROUP_UPDATE_AUCTION_STATUS_WHEN_IT_ENDED = "UPDATE_AUCTION_STATUS_WHEN_IT_ENDED";

    // Template
    public static final String TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDED_NORMAL = "Xin chào {0}! Phiên đấu giá {1} đã kết thúc!\n" +
            " Rất tiếc bạn đã không chiến thắng, chức may mắn trong lần đấu thầu khác!";
    public static final String TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDED_SELLER = "Xin chào {0}! Phiên đấu giá {1} của bạn đã kết thúc!\n" +
            " Người chiến thắng là {2} với giá thầu là {3} VND!";
    public static final String TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDED_SELLER_NO_BID = "Xin chào {0}! Phiên đấu giá {1} của bạn đã kết thúc" +
            " với 0 giao dịch đấu giá\nHãy đăng lại sản phẩm ở trang quản lý sản phẩm!\n";
    public static final String TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDED_WINNER = "Xin chào {0}! Phiên đấu giá {1} đã kết thúc!" +
            "\n Chúc mừng bạn là người chiến thắng với giá thầu {2}. Hãy vào trang quản lý đơn hàng để thanh toán nhé!";
    public static final String TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY = "Xin chào {0}! Phiên đấu giá {1} sắp kết thúc trong vòng 1 ngày nữa!";
    public static final String TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR = "Xin chào {0}! Phiên đấu giá {1} sắp kết thúc trong vòng 1 giờ nữa!";
    public static final String TEMPLATE_NOTIFICATION_CONFIRM_BID_SUCCESS = "Xin chào {0}! Bạn đã đặt giá thầu thành công ở sản phẩm {1} với già thầu tối đa: {2} VND!";
    public static final String TEMPLATE_NOTIFICATION_CONFIRM_OUTBID = "Xin chào {0}! Đã có người trả giá cao hơn giá thầu của bạn với giá thầu là {1} VND tại phiên đấu giá {2}!";
    public static final String TEMPLATE_NOTIFICATION_CONFIRM_AUCTION_ACCEPTED = "Xin chào {0} !Sản phẩm {1} đã được QTV eBOS chấp nhận!";
    public static final String TEMPLATE_NOTIFICATION_CONFIRM_AUCTION_REJECTED = " Xin chào {0}! Sản phẩm {1} không được chấp nhập bởi QTV eBOS.\nLý do: " +
            "{2}.\nNote: {3}";
    public static final String TEMPLATE_NOTIFICATION_ALERT_WEAK_MAX_BID = "Xin chào {0}! Giá thầu tối đa {1} của bạn có nguy cơ bị đánh bại!\nHãy tăng cơ hội chiến thắng bằng cách đặt" +
            " giá thầu tối đa hơn!";

    //History auction status
    public static final String HISTORY_STT_REJECTED_CANCELLED = "rejectedOrCancelled";
    public static final String HISTORY_STT_SELLING = "selling";
    public static final String HISTORY_STT_ENDED = "ended";
    public static final String HISTORY_STT_PENDING = "pending";
    public static final String HISTORY_STT_LOST = "lost";
    public static final String HISTORY_STT_BIDDING = "bidding";
    public static final String HISTORY_STT_WON = "won";

    // Time mode for chart
    public static final String TIME_MODE_DAILY = "daily";
    public static final String TIME_MODE_MONTHLY = "monthly";
    public static final String TIME_MODE_WEEKLY = "weekly";
    public static final String TIME_MODE_YEARLY = "yearly";

    public static final int MAX_SIZE_DATASET_DAILY = 30;
    public static final int MAX_SIZE_DATASET_WEEKLY = MAX_SIZE_DATASET_DAILY;
    public static final int MAX_SIZE_DATASET_MONTHLY = 21;

    // Exception
    public static final String EXCEPTION_NO_AUCTION_EXISTS = "No such auction exists";
    public static final String EXCEPTION_BID_LOWER_THAN_MINIMUM_BID = "Price must equals or be larger than current minimum bid";
    public static final String EXCEPTION_SELLER_BIDS_ON_OWN_ITEM = "Seller bids on his/her own item";
    public static final String EXCEPTION_NO_PERMISSION_TO_CONTACT_SELLER = "No permission to contact this seller. You're not winner of the auction";
    public static final String EXCEPTION_NO_PERMISSION_TO_CONTACT_WINNER = "No permission to contact this buyer. You're not seller of the item";
    public static final String EXCEPTION_ITEM_HAS_NO_WINNER = "Item has no winner";
    public static final String EXCEPTION_RESTRICTED_ACCOUNT = "Account is restricted!";
    public static final String EXCEPTION_ALREADY_ACTIVE_AUCTION = "Auction is already active";
    public static final String EXCEPTION_ALREADY_REJECTED_AUCTIONS = "Auction is already rejected";
    public static final String EXCEPTION_MEMBER_NOT_FOUND = "Member is not found";
    public static final String EXCEPTION_ACCOUNT_NOT_FOUND = "Account is not found";
    public static final String EXCEPTION_NEW_MAX_BID_LOWER_OLD_ONE = "You cant lower or repeat same maximum bid";
    public static final String EXCEPTION_MAX_BID_IS_TOO_HIGH = "Account is not found";

    // other
    public static final int REJECTION_LIMIT = 2;
    public static final int MAX_PRICE = 2_000_000_000;

}
