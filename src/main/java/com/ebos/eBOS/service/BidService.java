package com.ebos.eBOS.service;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.ChartResponse;
import com.ebos.eBOS.data.entity.Auction;
import com.ebos.eBOS.data.entity.Bid;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.exception.AuctionNotFoundException;
import com.ebos.eBOS.exception.InvalidBidPriceException;
import com.ebos.eBOS.exception.InvalidRequestException;
import com.ebos.eBOS.exception.SellerBidsOnOwnItemException;
import com.ebos.eBOS.repository.AuctionRepository;
import com.ebos.eBOS.repository.BidRepository;
import com.ebos.eBOS.service.firebase.FcmService;
import com.ebos.eBOS.util.DateTimeUtils;
import com.ebos.eBOS.util.MinIncrementUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BidService {

    private BidRepository bidRepository;
    private AuctionRepository auctionRepository;
    private AuctionService auctionService;
    private FcmService fcmService;

    public BidService(BidRepository bidRepository,
                      AuctionRepository auctionRepository,
                      AuctionService auctionService,
                      FcmService fcmService) {
        this.bidRepository = bidRepository;
        this.auctionRepository = auctionRepository;
        this.auctionService = auctionService;
        this.fcmService = fcmService;
    }

    public void addBid(Integer auctionId, User bidder, BigDecimal maxPrice) throws InvalidRequestException, AuctionNotFoundException {
        Auction auction = auctionRepository.findByIdAndStatus(auctionId, AppConstants.AUCTION_STT_ACTIVE);
        checkValidInputBeforeAddingBid(auction, bidder, maxPrice);

        if (auction.getCurrentHighestBid() == null) {
            addBidWhenAuctionHasNoBid(auction, bidder, maxPrice);
        } else {
            User currentWinner = auction.getCurrentHighestBid().getBidder();
            if (bidder.getId().equals(currentWinner.getId())) {
                if (maxPrice.compareTo(auction.getCurrentMinimumBidForHighestBidder()) < 0) {
                    throw new InvalidBidPriceException(AppConstants.EXCEPTION_NEW_MAX_BID_LOWER_OLD_ONE);
                }
                addBidWhenBidderIsCurrentWinner(auction, bidder, maxPrice);
            } else {
                Bid highestBid = auction.getCurrentHighestBid();
                if (maxPrice.compareTo(highestBid.getMaxPrice()) <= 0) {
                    addBidWhenNormalBidderBidsEqualOrLowerThanHighestBid(auction, bidder, maxPrice);
                } else {
                    addBidWhenNormalBidderBidsHigherThanHighestBid(auction, bidder, maxPrice);
                }
            }
        }
    }


    private void addBidWhenAuctionHasNoBid(Auction auction, User bidder, BigDecimal maxPrice) {
        Bid bid = createBid(auction, bidder, auction.getCurrentMinimumBid(), maxPrice, false);
        bid = bidRepository.save(bid);

        auction = auctionService.updateAuction(auction, auction.getCurrentMinimumBid(),
                MinIncrementUtils.addIncrement(auction.getCurrentMinimumBid()),
                MinIncrementUtils.addIncrement(maxPrice), 1, bid);

        List<Bid> bids = new ArrayList<>();
        bids.add(bid);
        auction.setBids(bids);
        auctionService.saveToFirebase(auction);

        String body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_CONFIRM_BID_SUCCESS, bidder.getUsername(),
                auction.getItem().getName(), maxPrice);
        Map<String, String> data = new HashMap<>();
        data.put(AppConstants.N_DATA_AUCTION_ID, auction.getId() + "");
        fcmService.sendMessageToUser(bidder.getId(), AppConstants.N_TITLE_DEFAULT, body, data);
        System.out.println("No bid: " + bidder.getUsername());
        auctionService.setUpScheduleNotificationToUserWhenAuctionEndingSoon(auction, bidder);
    }

    private void addBidWhenBidderIsCurrentWinner(Auction auction, User bidder, BigDecimal maxPrice) {
        Bid oldHighestBid = auction.getCurrentHighestBid();
        oldHighestBid.setHidden(true);
        Bid bid = createBid(auction, bidder, oldHighestBid.getPrice(), maxPrice, false);

        BigDecimal currentPrice = auction.getCurrentPrice();
        BigDecimal currentMinBid = auction.getCurrentMinimumBid();
        int numberOfBids = auction.getNumberOfBids();

        if (oldHighestBid.getMaxPrice().compareTo(auction.getCurrentMinimumBid()) < 0 && oldHighestBid.isAuto()) {
            bid = createBid(auction, bidder, auction.getCurrentMinimumBid(), maxPrice, false);
            currentPrice = auction.getCurrentMinimumBid();
            currentMinBid = MinIncrementUtils.addIncrement(currentPrice);
            numberOfBids = auction.getNumberOfBids() + 1;
            oldHighestBid.setHidden(false);
        }
        bidRepository.save(oldHighestBid);
        bid = bidRepository.save(bid);
        auction = auctionService.updateAuction(auction, currentPrice, currentMinBid,
                MinIncrementUtils.addIncrement(maxPrice), numberOfBids, bid);

        auction.getBids().add(bid);
        auctionService.saveToFirebase(auction);

        String body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_CONFIRM_BID_SUCCESS, bidder.getUsername(),
                auction.getItem().getName(), maxPrice);
        Map<String, String> data = new HashMap<>();
        data.put(AppConstants.N_DATA_AUCTION_ID, auction.getId() + "");

        fcmService.sendMessageToUser(bidder.getId(), AppConstants.N_TITLE_DEFAULT, body, data);
    }

    private void addBidWhenNormalBidderBidsEqualOrLowerThanHighestBid(Auction auction, User bidder, BigDecimal maxPrice) {
        Bid highestBid = auction.getCurrentHighestBid();
        BigDecimal highestBidMaxPrice = highestBid.getMaxPrice();
        BigDecimal tempPrice = auction.getCurrentMinimumBid();

        //equals
        if (maxPrice.compareTo(highestBidMaxPrice) == 0) {
            tempPrice = highestBidMaxPrice;
            // lower
        } else {
//            while (tempPrice.compareTo(highestBidMaxPrice) <= 0 && tempPrice.compareTo(maxPrice) <= 0) {
//                if (MinIncrementUtils.addIncrement(tempPrice).compareTo(highestBidMaxPrice) <= 0) {
//                    tempPrice = MinIncrementUtils.addIncrement(tempPrice);
//                }
//            }
            if (MinIncrementUtils.addIncrement(maxPrice).compareTo(highestBidMaxPrice) > 0) {
                tempPrice = highestBidMaxPrice;
            } else {
                tempPrice = MinIncrementUtils.addIncrement(maxPrice);
            }
        }

        Bid normalBid = createBid(auction, bidder, maxPrice, maxPrice, false);
        Bid autobidForHighest = createBid(auction, highestBid.getBidder(), tempPrice, highestBidMaxPrice, true);

        // equals
        if (maxPrice.compareTo(highestBidMaxPrice) == 0) {
            ZonedDateTime createdDate = autobidForHighest.getCreatedDate();
            autobidForHighest.setCreatedDate(createdDate.minusSeconds(1));
        }

        if (maxPrice.compareTo(highestBidMaxPrice) == 0) {
            autobidForHighest = bidRepository.save(autobidForHighest);
            normalBid = bidRepository.save(normalBid);

            auction.getBids().add(autobidForHighest);
            auction.getBids().add(normalBid);
        } else {
            normalBid = bidRepository.save(normalBid);
            autobidForHighest = bidRepository.save(autobidForHighest);

            auction.getBids().add(normalBid);
            auction.getBids().add(autobidForHighest);
        }

        int numOfBids = auction.getNumberOfBids();
        auction = auctionService.updateAuction(auction, autobidForHighest.getPrice(),
                MinIncrementUtils.addIncrement(autobidForHighest.getPrice()), auction.getCurrentMinimumBidForHighestBidder(),
                numOfBids + 2, autobidForHighest);

        auctionService.saveToFirebase(auction);

        String body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_CONFIRM_OUTBID, bidder.getUsername(),
                autobidForHighest.getPrice(), auction.getItem().getName());
        Map<String, String> data = new HashMap<>();
        data.put(AppConstants.N_DATA_AUCTION_ID, auction.getId() + "");
        fcmService.sendMessageToUser(bidder.getId(), AppConstants.N_TITLE_DEFAULT, body, data);

        if (checkAutoBidMaxPriceWeak(autobidForHighest, auction)) {
            body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_ALERT_WEAK_MAX_BID,
                    autobidForHighest.getBidder().getUsername(),
                    autobidForHighest.getMaxPrice());
            data = new HashMap<>();
            data.put(AppConstants.N_DATA_AUCTION_ID, auction.getId() + "");
            fcmService.sendMessageToUser(autobidForHighest.getBidder().getId(), AppConstants.N_TITLE_DEFAULT, body, data);
        }

        if (checkUserBidsOnAuctionFirstTime(bidder, auction)) {
            System.out.println("Lower: " + bidder.getUsername());
            auctionService.setUpScheduleNotificationToUserWhenAuctionEndingSoon(auction, bidder);
        }
    }

    private void addBidWhenNormalBidderBidsHigherThanHighestBid(Auction auction, User bidder, BigDecimal maxPrice) {
        Bid highestBid = auction.getCurrentHighestBid();
        BigDecimal oldHighestBidMaxPrice = highestBid.getMaxPrice();

        Bid bid;

        if (oldHighestBidMaxPrice.compareTo(auction.getCurrentMinimumBid()) > 0) {
            bid = createBid(auction, bidder, oldHighestBidMaxPrice, maxPrice, false);

            auction.setCurrentPrice(oldHighestBidMaxPrice);
            auction.setCurrentMinimumBid(MinIncrementUtils.addIncrement(auction.getCurrentPrice()));
        } else {
            bid = createBid(auction, bidder, auction.getCurrentMinimumBid(), maxPrice, false);

            auction.setCurrentPrice(auction.getCurrentMinimumBid());
            auction.setCurrentMinimumBid(MinIncrementUtils.addIncrement(auction.getCurrentPrice()));
        }

        bidRepository.save(bid);

        auction.setCurrentMinimumBidForHighestBidder(MinIncrementUtils.addIncrement(maxPrice));
        int numOfBids = auction.getNumberOfBids();
        auction.setNumberOfBids(++numOfBids);
        auction.setCurrentHighestBid(bid);
        auctionService.save(auction);

        auction.getBids().add(bid);
        auctionService.saveToFirebase(auction);

        String body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_CONFIRM_OUTBID, highestBid.getBidder().getUsername(),
                bid.getPrice(), auction.getItem().getName());
        Map<String, String> data = new HashMap<>();
        data.put(AppConstants.N_DATA_AUCTION_ID, auction.getId() + "");
        fcmService.sendMessageToUser(highestBid.getBidder().getId(), AppConstants.N_TITLE_DEFAULT, body, data);

        body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_CONFIRM_BID_SUCCESS, bidder.getUsername(),
                auction.getItem().getName(), maxPrice);
        fcmService.sendMessageToUser(bidder.getId(), AppConstants.N_TITLE_DEFAULT, body, data);


        if (checkUserBidsOnAuctionFirstTime(bidder, auction)) {
            System.out.println("Higher: " + bidder.getUsername());
            auctionService.setUpScheduleNotificationToUserWhenAuctionEndingSoon(auction, bidder);
        }
    }

    private boolean checkAutoBidMaxPriceWeak(Bid autoBid, Auction auction) {
        BigDecimal strength = auction.getCurrentMinimumBid().subtract(autoBid.getMaxPrice());
        BigDecimal increment = MinIncrementUtils.calculateIncrement(auction.getCurrentPrice());

        return strength.compareTo(new BigDecimal(0)) >= 0 && strength.compareTo(increment) <= 0;
    }

    private boolean checkUserBidsOnAuctionFirstTime(User bidder, Auction auction) {
        boolean result = bidRepository.countByBidderIdAndAuctionId(bidder.getId(), auction.getId()) == 1;
        System.out.println("First time " + result);
        return result;
    }

    private Bid createBid(Auction auction, User bidder, BigDecimal price, BigDecimal maxPrice, boolean isAuto) {
        Bid bid = new Bid();
        bid.setAuction(auction);
        bid.setBidder(bidder);
        bid.setCreatedDate(ZonedDateTime.now());
        bid.setPrice(price);
        bid.setMaxPrice(maxPrice);
        bid.setAuto(isAuto);
        bid.setHidden(false);
        return bid;
    }

    private void checkValidInputBeforeAddingBid(Auction auction, User bidder, BigDecimal price) throws AuctionNotFoundException,
            InvalidRequestException {
        if (bidder.getStatus().equals(AppConstants.USER_STT_RESTRICTED)) {
            throw new InvalidRequestException(AppConstants.EXCEPTION_RESTRICTED_ACCOUNT);
        }
        if (auction == null) {
            throw new AuctionNotFoundException();
        }
        if (bidder.getId().equals(auction.getItem().getSeller().getId())) {
            throw new SellerBidsOnOwnItemException();
        }
        if (price.compareTo(auction.getCurrentMinimumBid()) < 0) {
            throw new InvalidBidPriceException(AppConstants.EXCEPTION_BID_LOWER_THAN_MINIMUM_BID);
        }
        BigDecimal increment = MinIncrementUtils.calculateIncrement(auction.getCurrentPrice());
        if (price.compareTo(increment.multiply(new BigDecimal(1000))) >= 0) {
            throw new InvalidBidPriceException(AppConstants.EXCEPTION_MAX_BID_IS_TOO_HIGH);
        }
    }

    public ChartResponse getDailyCountBidsByChart() {
        List<Integer> counts = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        ZonedDateTime from = DateTimeUtils.getStartOfToDay();
        ZonedDateTime to = DateTimeUtils.getEndOfToDay();
        for (int i = AppConstants.MAX_SIZE_DATASET_DAILY; i > 0; i--) {
            int count = getCountBidsByCreatedDateBetween(from, to);
            String label = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(to);
            counts.add(count);
            labels.add(label);
            from = from.minusDays(1);
            to = to.minusDays(1);
        }

        ChartResponse response = new ChartResponse();
        response.setDataSet(counts);
        response.setLabels(labels);
        return response;
    }

    public ChartResponse getWeeklyCountBidsByChart() {
        List<Integer> counts = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        ZonedDateTime from = DateTimeUtils.getStartOfDateTime(ZonedDateTime.now().with(TemporalAdjusters.previous(DayOfWeek.MONDAY)));
        ZonedDateTime to = DateTimeUtils.getEndOfDateTime(from.plusDays(6));

        for (int i = AppConstants.MAX_SIZE_DATASET_WEEKLY; i > 0; i--) {
            int count = getCountBidsByCreatedDateBetween(from, to);
            String label = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(from);
            counts.add(count);
            labels.add(label);
            from = from.minusDays(7);
            to = to.minusDays(7);
        }

        ChartResponse response = new ChartResponse();
        response.setDataSet(counts);
        response.setLabels(labels);
        return response;
    }

    public ChartResponse getMonthlyCountBidsByChart() {
        List<Integer> counts = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        ZonedDateTime from = DateTimeUtils.getStartOfDateTime(ZonedDateTime.now().with(TemporalAdjusters.firstDayOfMonth()));
        ZonedDateTime to = DateTimeUtils.getEndOfDateTime(from.with(TemporalAdjusters.lastDayOfMonth()));

        for (int i = AppConstants.MAX_SIZE_DATASET_MONTHLY; i > 0; i--) {
            int count = getCountBidsByCreatedDateBetween(from, to);
            String label = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(from);
            counts.add(count);
            labels.add(label);

            to = DateTimeUtils.getEndOfDateTime(from.minusDays(1));
            from = DateTimeUtils.getStartOfDateTime(to.with(TemporalAdjusters.firstDayOfMonth()));
        }

        ChartResponse response = new ChartResponse();
        response.setDataSet(counts);
        response.setLabels(labels);
        return response;
    }

    private Integer getCountBidsByCreatedDateBetween(ZonedDateTime from, ZonedDateTime to) {
        return bidRepository.countAllByCreatedDateBetweenAndIsAutoFalse(from, to);
    }
}
