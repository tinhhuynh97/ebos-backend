package com.ebos.eBOS.service;

import com.ebos.eBOS.data.entity.Item;
import com.ebos.eBOS.repository.ItemRepository;
import org.springframework.stereotype.Service;

@Service
public class ItemService {

    private ItemRepository itemRepository;

    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public Item findById(int id) {
        return itemRepository.findById(id).orElse(null);
    }
}
