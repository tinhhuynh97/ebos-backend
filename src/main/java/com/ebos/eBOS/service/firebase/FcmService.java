package com.ebos.eBOS.service.firebase;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.entity.Auction;
import com.ebos.eBOS.data.entity.Bid;
import com.ebos.eBOS.data.entity.FcmToken;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.repository.FcmTokenRepository;
import com.google.firebase.messaging.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class FcmService {

    private static final int TTL_1_WEEK = 3600 * 1000 * 24 * 7;
    private static final int DAYS_IN_WEEK = 7;

    private FcmTokenRepository fcmTokenRepository;

    public FcmService(FcmTokenRepository fcmTokenRepository) {
        this.fcmTokenRepository = fcmTokenRepository;
    }

    public void addToken(FcmToken fcmToken) {
        fcmTokenRepository.save(fcmToken);
    }

    public void delete(FcmToken fcmToken) {
        fcmTokenRepository.delete(fcmToken);
    }

    public FcmToken findTokenByUserAndValue(FcmToken fcmToken) {
        return fcmTokenRepository.findByUserIdAndValue(fcmToken.getUser().getId(), fcmToken.getValue());
    }

    public FcmToken findTokenByValue(String value) {
        return fcmTokenRepository.findByValue(value);
    }

    public boolean checkTokenExists(FcmToken fcmToken) {
        return fcmTokenRepository.existsByUserIdAndValue(fcmToken.getUser().getId(), fcmToken.getValue());
    }

    public List<FcmToken> findAllByUser(Integer userId) {
        return fcmTokenRepository.findByUserId(userId);
    }

    public void sendMessageToUser(Integer userId, String title, String body, Map<String, String> data) {
        List<FcmToken> fcmTokens = findAllByUser(userId);
        List<String> tokens = fcmTokens.stream().map(FcmToken::getValue).collect(Collectors.toList());
        sendMessages(title, body, data, tokens);
    }

    public void sendMessage(String title, String body, Map<String, String> data, String token) {
        String response = null;
        Message message = buildMessage(title, body, data, token);
        try {
            response = FirebaseMessaging.getInstance().send(message);
            // Response is a message ID string.
            System.out.println("Successfully sent message: " + response);
        } catch (FirebaseMessagingException e) {
            System.out.println(e.getErrorCode());
            handleFcmException(e, token);

        }
    }

    public void sendMessages(String title, String body, Map<String, String> data, List<String> tokens) {
        tokens.forEach(token -> {
            Message message = buildMessage(title, body, data, token);
            try {
                FirebaseMessaging.getInstance().send(message);
            } catch (FirebaseMessagingException e) {
                System.out.println(e.getErrorCode());
                handleFcmException(e, token);
            }
        });
    }

    private void handleFcmException(FirebaseMessagingException e, String token) {
        if (e.getErrorCode().contains("registration-token-not-registered")) {
            FcmToken fcmToken = findTokenByValue(token);
            if (token != null) {
                delete(fcmToken);
            }
        } else {
            e.printStackTrace();
        }
    }

    private Message buildMessage(String title, String body, Map<String, String> data, String token) {
        LocalDate localDate = LocalDate.now();
        LocalDate later = localDate.plusDays(DAYS_IN_WEEK);
        Message.Builder builder = Message.builder()
                .setNotification(new Notification(
                        title,
                        body))
                .setAndroidConfig(AndroidConfig.builder()
                        .setTtl(TTL_1_WEEK)
                        .build())
                .setApnsConfig(ApnsConfig.builder()
                        .setAps(Aps.builder()
                                .setBadge(0)
                                .build())
                        .putHeader("apns-expiration", String.valueOf(later.toEpochDay()))
                        .build())
                .setWebpushConfig(WebpushConfig.builder()
                        .putHeader("TTL", DAYS_IN_WEEK + "")
                        .build())
                .setToken(token);

        if (!CollectionUtils.isEmpty(data)) {
            builder.putAllData(data);
        }

        return builder.build();
    }

    public void sendMessageToUsersFromAuction(Auction auction) {
        Map<String, String> data = new HashMap<>();
        data.put(AppConstants.N_DATA_AUCTION_ID, auction.getId() + "");
//        auction.setBids(new ArrayList<>(auction.getBids()));
        List<User> users = getUsersFromAuction(auction);
        User seller = auction.getItem().getSeller();
        User winner = null;
        if (auction.getCurrentHighestBid() != null) {
            winner = auction.getCurrentHighestBid().getBidder();
        }
        User finalWinner = winner;
        users.forEach(user -> {
            String body = "";
            if (user.getId().equals(seller.getId())) {
                if (CollectionUtils.isEmpty(auction.getBids())) {
                    body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDED_SELLER_NO_BID,
                            seller.getUsername(),
                            auction.getItem().getName());
                } else {
                    if (finalWinner != null) {
                        body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDED_SELLER,
                                seller.getUsername(),
                                auction.getItem().getName(),
                                finalWinner.getUsername(),
                                auction.getCurrentHighestBid().getPrice());
                    }
                }
            } else if (finalWinner != null && user.getId().equals(finalWinner.getId())) {
                body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDED_WINNER,
                        user.getUsername(),
                        auction.getItem().getName(),
                        auction.getCurrentHighestBid().getPrice());
            } else if (finalWinner != null) {
                body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDED_NORMAL,
                        user.getUsername(),
                        auction.getItem().getName());
            }
            sendMessageToUser(user.getId(), AppConstants.N_TITLE_DEFAULT, body, data);
        });
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    private List<User> getUsersFromAuction(Auction auction) {

        List<User> users = new ArrayList<>();
        User seller = auction.getItem().getSeller();
        users.add(seller);
        if (!CollectionUtils.isEmpty(auction.getBids())) {
            Collection<User> bidders = auction.getBids().stream()
                    .map(Bid::getBidder)
                    .filter(distinctByKey(User::getId))
                    .collect(Collectors.toList());
            users.addAll(bidders);
        }
        return users;
    }

}
