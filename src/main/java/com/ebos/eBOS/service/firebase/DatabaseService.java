package com.ebos.eBOS.service.firebase;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.firebase.db.FbAuction;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.stereotype.Service;


@Service
public class DatabaseService {

    public FirebaseDatabase getDatabase() {
        return FirebaseDatabase.getInstance();
    }

    public DatabaseReference getAuctionsRef() {
        return getDatabase().getReference(AppConstants.FIREBASE_AUCTIONS_PATH);
    }

    public void save(FbAuction auction) {
        DatabaseReference audtionsRef = getAuctionsRef();
        DatabaseReference audtionNode = audtionsRef.child(auction.getId());
        audtionNode.setValueAsync(auction);
    }
}
