package com.ebos.eBOS.service;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.VNTranslator;
import com.ebos.eBOS.data.AuctionAffinity;
import com.ebos.eBOS.data.AuctionStatistics;
import com.ebos.eBOS.data.ChartResponse;
import com.ebos.eBOS.data.entity.*;
import com.ebos.eBOS.data.firebase.db.FbAuction;
import com.ebos.eBOS.exception.AuctionNotFoundException;
import com.ebos.eBOS.exception.InvalidRequestException;
import com.ebos.eBOS.repository.AuctionRepository;
import com.ebos.eBOS.repository.BrowseRepository;
import com.ebos.eBOS.repository.ItemRepository;
import com.ebos.eBOS.repository.UserRepository;
import com.ebos.eBOS.service.firebase.DatabaseService;
import com.ebos.eBOS.service.firebase.FcmService;
import com.ebos.eBOS.service.quartz.QuartzService;
import com.ebos.eBOS.service.quartz.job.EndAuctionJob;
import com.ebos.eBOS.service.quartz.job.NotificationJob;
import com.ebos.eBOS.util.DateTimeUtils;
import com.ebos.eBOS.util.MinIncrementUtils;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class AuctionService {

    private ItemRepository itemRepository;
    private AuctionRepository auctionRepository;
    private MediaService mediaService;
    private BrowseRepository browseRepository;
    private UserRepository userRepository;

    private DatabaseService fbDatabaseService;
    private FcmService fcmService;
    private QuartzService quartzService;

    public AuctionService(ItemRepository itemRepository, AuctionRepository auctionRepository, MediaService mediaService, BrowseRepository browseRepository, UserRepository userRepository, DatabaseService fbDatabaseService, FcmService fcmService, QuartzService quartzService) {
        this.itemRepository = itemRepository;
        this.auctionRepository = auctionRepository;
        this.mediaService = mediaService;
        this.browseRepository = browseRepository;
        this.userRepository = userRepository;
        this.fbDatabaseService = fbDatabaseService;
        this.fcmService = fcmService;
        this.quartzService = quartzService;
    }

    public void add(Auction auction, List<MultipartFile> images) throws InvalidRequestException {
        User seller = auction.getItem().getSeller();
        // check seller is restricted or not
        if (seller.getStatus().equals(AppConstants.USER_STT_RESTRICTED)) {
            throw new InvalidRequestException(AppConstants.EXCEPTION_RESTRICTED_ACCOUNT);
        }
        Item item = auction.getItem();
        item.setCreatedDate(ZonedDateTime.now());
        itemRepository.save(item);
        auction.setItem(item);
        auction.setCreatedDate(ZonedDateTime.now());
        auction.setStatus(AppConstants.AUCTION_STT_PENDING_FOR_ACCEPTING);
        auctionRepository.save(auction);
        mediaService.addAll(item, images);
    }

    public Auction save(Auction auction) {
        return auctionRepository.save(auction);
    }

    public void save(Auction auction, boolean needUpdateToFirebase) {
        auction = auctionRepository.save(auction);
        if (needUpdateToFirebase) {
            fbDatabaseService.save(FbAuction.from(auction));
        }
    }

    public List<Auction> findAllActive() {
        return auctionRepository.findAllByStatus(AppConstants.AUCTION_STT_ACTIVE);
    }

    public void saveToFirebase(Auction auction) {
        FbAuction fbAuction = FbAuction.from(auction);
        fbDatabaseService.save(fbAuction);
    }

    public Page<Auction> getPendingAuctions(Pageable pageable) {
        return auctionRepository.findAllByStatusOrderByCreatedDateAsc(AppConstants.AUCTION_STT_PENDING_FOR_ACCEPTING, pageable);
    }

    public void acceptAuctions(Integer id, User admin) throws InvalidRequestException {
        Auction auction = auctionRepository.findById(id).orElse(null);
        if (auction != null) {
            if (auction.getStatus().equals(AppConstants.AUCTION_STT_ACTIVE)) {
                throw new InvalidRequestException(AppConstants.EXCEPTION_ALREADY_ACTIVE_AUCTION);
            }
            if (auction.getStatus().equals(AppConstants.AUCTION_STT_REJECTED)) {
                throw new InvalidRequestException(AppConstants.EXCEPTION_ALREADY_REJECTED_AUCTIONS);
            }
            auction.setStatus(AppConstants.AUCTION_STT_ACTIVE);
            auction.setCurrentPrice(auction.getReservePrice());
            auction.setCurrentMinimumBid(MinIncrementUtils.addIncrement(auction.getCurrentPrice()));
            auction.setStartTime(ZonedDateTime.now());
            auction.setPlannedEndTime(auction.getStartTime().plusDays(auction.getDurationInDay()));
            auction.setLastModifiedDate(ZonedDateTime.now());
            auction.setLastModifiedBy(admin);
            auctionRepository.save(auction);
            saveToFirebase(auction);

            String title = AppConstants.N_TITLE_DEFAULT;
            String body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_CONFIRM_AUCTION_ACCEPTED,
                    auction.getItem().getSeller().getUsername(),
                    auction.getItem().getName());

            sendNotificationToSeller(auction, title, body);

            scheduleUpdateAuctionStatusWhenEnded(auction);

            User seller = auction.getItem().getSeller();

            setUpScheduleNotificationToUserWhenAuctionEndingSoon(auction, seller);
        }
    }

    public void setUpScheduleNotificationToUserWhenAuctionEndingSoon(Auction auction, User user) {
        if (auction.getDurationInDay() > 1) {
            scheduleNotificationToUserWhenAuctionEndingSoonInOneDay(auction, user);
        }
        scheduleNotificationToUserWhenAuctionEndingSoonInOneHour(auction, user);
    }

    private void scheduleUpdateAuctionStatusWhenEnded(Auction auction) {
        ZonedDateTime triggerDate = auction.getStartTime().plusDays(auction.getDurationInDay());
//        ZonedDateTime triggerDate = auction.getStartTime().plusMinutes(5);

        String jobKey = MessageFormat.format(AppConstants.JOB_KEY_UPDATE_AUCTION_STATUS_WHEN_IT_ENDED, auction.getId());

        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(AppConstants.JMD_AUCTION_ID, "" + auction.getId());

        JobDetail jobDetail = quartzService.buildJobDetail(EndAuctionJob.class, jobKey,
                AppConstants.JOB_GROUP_UPDATE_AUCTION_STATUS_WHEN_IT_ENDED, "", jobDataMap);

        Trigger trigger = quartzService.buildJobTrigger(jobDetail,
                AppConstants.TRIGGER_GROUP_UPDATE_AUCTION_STATUS_WHEN_IT_ENDED, "",
                triggerDate);

        quartzService.scheduleJob(jobDetail, trigger);
    }

    private void scheduleNotificationToUserWhenAuctionEndingSoonInOneDay(Auction auction, User user) {
        ZonedDateTime triggerDate = auction.getStartTime().plusDays(auction.getDurationInDay()).minusDays(1);
//        ZonedDateTime triggerDate = auction.getStartTime().plusMinutes(3);
        String jobKey = MessageFormat.format(AppConstants.JOB_KEY_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY, user.getId(), auction.getId());
        String jobGroup = AppConstants.JOB_GROUP_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY;
        String triggerGroup = AppConstants.TRIGGER_GROUP_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY;

        scheduleNotificationToUserWhenAuctionEndingSoon(auction, user,
                AppConstants.TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_DAY,
                jobKey, jobGroup, triggerGroup,
                triggerDate);
    }

    private void scheduleNotificationToUserWhenAuctionEndingSoonInOneHour(Auction auction, User user) {
        ZonedDateTime triggerDate = auction.getStartTime().plusDays(auction.getDurationInDay()).minusHours(1);
//        ZonedDateTime triggerDate = auction.getStartTime().plusMinutes(4);
        String jobKey = MessageFormat.format(AppConstants.JOB_KEY_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR, user.getId(), auction.getId());
        String jobGroup = AppConstants.JOB_GROUP_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR;
        String triggerGroup = AppConstants.TRIGGER_GROUP_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR;


        scheduleNotificationToUserWhenAuctionEndingSoon(auction, user,
                AppConstants.TEMPLATE_NOTIFICATION_WHEN_AUCTION_ENDS_IN_ONE_HOUR,
                jobKey, jobGroup, triggerGroup,
                triggerDate);
    }

    private void scheduleNotificationToUserWhenAuctionEndingSoon(Auction auction, User user, String bodyTemplate,
                                                                 String jobKey, String jobGroup, String triggerGroup,
                                                                 ZonedDateTime triggerDate) {
        int userId = user.getId();

        String body = MessageFormat.format(bodyTemplate,
                user.getUsername(), auction.getItem().getName());

        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(AppConstants.JMD_NOTIFICATION_TITLE, AppConstants.N_TITLE_DEFAULT);
        jobDataMap.put(AppConstants.JMD_NOTIFICATION_BODY, body);
        jobDataMap.put(AppConstants.JMD_NOTIFICATION_USER_ID, "" + userId);
        jobDataMap.put(AppConstants.JMD_NOTIFICATION_DATA_AUCTION_ID, "" + auction.getId());

        JobDetail jobDetail = quartzService.buildJobDetail(NotificationJob.class, jobKey,
                jobGroup, "", jobDataMap);

        Trigger trigger = quartzService.buildJobTrigger(jobDetail,
                triggerGroup, "",
                triggerDate);

        quartzService.scheduleJob(jobDetail, trigger);
    }


    public void rejectAuction(Integer id, String reason, String note, User admin) throws InvalidRequestException {
        Auction auction = auctionRepository.findById(id).orElse(null);
        if (auction != null) {
//            Integer numOfRejection = auction.getNumOfRejection();
//
//            if (numOfRejection == null || numOfRejection < AppConstants.REJECTION_LIMIT) {
//                auction.setStatus(AppConstants.AUCTION_STT_REJECTED);
//                numOfRejection = numOfRejection == null ? 1 : ++numOfRejection;
//                auction.setNumOfRejection(numOfRejection);
//            } else {
//                auction.setStatus(AppConstants.AUCTION_STT_CANCELLED_BY_ADMIN);
//            }
            if (auction.getStatus().equals(AppConstants.AUCTION_STT_ACTIVE)) {
                throw new InvalidRequestException(AppConstants.EXCEPTION_ALREADY_ACTIVE_AUCTION);
            }
            if (auction.getStatus().equals(AppConstants.AUCTION_STT_REJECTED)) {
                throw new InvalidRequestException(AppConstants.EXCEPTION_ALREADY_REJECTED_AUCTIONS);
            }
            auction.setStatus(AppConstants.AUCTION_STT_REJECTED);
            auction.setRejectedReason(reason);
            auction.setNote(note);
            auction.setLastModifiedDate(ZonedDateTime.now());
            auction.setLastModifiedBy(admin);
            auctionRepository.save(auction);

            String title = AppConstants.N_TITLE_DEFAULT;
            String body = MessageFormat.format(AppConstants.TEMPLATE_NOTIFICATION_CONFIRM_AUCTION_REJECTED,
                    auction.getItem().getSeller().getUsername(),
                    auction.getItem().getName(),
                    VNTranslator.getInstance().get(reason),
                    note);

            sendNotificationToSeller(auction, title, body);
        }
    }

    private void sendNotificationToSeller(Auction auction, String title, String body) {
        Integer sellerId = auction.getItem().getSeller().getId();
        Map<String, String> data = new HashMap<>();
        data.put(AppConstants.N_DATA_AUCTION_ID, auction.getId() + "");
        fcmService.sendMessageToUser(sellerId, title, body, data);
    }


    public Page<Auction> getActiveAuctionsByCategory(Integer categoryId, BigDecimal minPrice, BigDecimal maxPrice, Pageable pageable) {
        if (minPrice == null) {
            minPrice = BigDecimal.valueOf(0);
        }
        if (maxPrice == null) {
            maxPrice = BigDecimal.valueOf(AppConstants.MAX_PRICE);
        }
        Page<Auction> auctionPage = auctionRepository.findAllByCategoryIdAndStatus(categoryId,
                AppConstants.AUCTION_STT_ACTIVE, minPrice, maxPrice, pageable);
        auctionPage.getContent().forEach(this::addTimeLeft);
        return auctionPage;
    }

    public Page<Auction> getActiveAuctionsByKeyword(String keyword, BigDecimal minPrice, BigDecimal maxPrice, Pageable pageable) {
        if (minPrice == null) {
            minPrice = BigDecimal.valueOf(0);
        }
        if (maxPrice == null) {
            maxPrice = BigDecimal.valueOf(AppConstants.MAX_PRICE);
        }
        Page<Auction> auctionPage = auctionRepository.findAllByKeywordAndStatus(keyword,
                AppConstants.AUCTION_STT_ACTIVE, minPrice, maxPrice, pageable);
        auctionPage.getContent().forEach(this::addTimeLeft);
        return auctionPage;
    }


    public Auction getActiveAuctionById(Integer id) {
        Auction auction = auctionRepository.findByIdAndStatus(id, AppConstants.AUCTION_STT_ACTIVE);
        if (auction != null) {
            addTimeLeft(auction);
        }
        return auction;
    }

    public Auction getEndedAuctionById(Integer id) {
        return auctionRepository.findByIdAndStatus(id, AppConstants.AUCTION_STT_ENDED);
    }

    private void addTimeLeft(Auction auction) {
        long timeLeft = DateTimeUtils.calculateTimeLeft(auction.getPlannedEndTime());
        auction.setTimeLeft(timeLeft);
    }

    public Auction getActiveAuctionByIdForUser(Integer id, User user) {
        Auction auction = getActiveAuctionById(id);
        if (auction != null && auction.getCurrentHighestBid() != null) {
            User winner = auction.getCurrentHighestBid().getBidder();
            if (user.getId().equals(winner.getId())) {
                auction.setCurrentMinimumBid(auction.getCurrentMinimumBidForHighestBidder());
            }
        }
        return auction;
    }

    public Auction updateAuction(Auction auction, BigDecimal currentPrice, BigDecimal minBid,
                                 BigDecimal minBidForHighestBidder, int numberOfBids, Bid highestBid) {
        auction.setCurrentPrice(currentPrice);
        auction.setCurrentMinimumBid(minBid);
        auction.setCurrentMinimumBidForHighestBidder(minBidForHighestBidder);
        auction.setNumberOfBids(numberOfBids);
        auction.setCurrentHighestBid(highestBid);
        auction = save(auction);
        return auction;
    }

    public List<Auction> getActiveAuctions() {
        return auctionRepository.findAllByStatus(AppConstants.AUCTION_STT_ACTIVE);
    }

    public List<BigDecimal> getSuggestedBidPrice(Integer auctionId) throws AuctionNotFoundException {
        Auction auction = getActiveAuctionById(auctionId);
        if (auction == null) {
            throw new AuctionNotFoundException();
        }
        List<BigDecimal> prices = new ArrayList<>();
        BigDecimal firstPrice = auction.getCurrentMinimumBid();
        BigDecimal secondPrice = MinIncrementUtils.addIncrement(firstPrice);
        BigDecimal thirdPrice = secondPrice.add(MinIncrementUtils.calculateIncrement(secondPrice).multiply(BigDecimal.valueOf(3)));

        prices.add(firstPrice);
        prices.add(secondPrice);
        prices.add(thirdPrice);

        return prices;
    }

    public Page<Auction> getRejectedOrCanceledAuctionsBySeller(User seller, Pageable pageable) {
        List<String> statues = Arrays.asList(AppConstants.AUCTION_STT_REJECTED,
                AppConstants.AUCTION_STT_CANCELLED_BY_ADMIN,
                AppConstants.AUCTION_STT_CANCELLED_BY_SELLER);

        return auctionRepository.findDistinctByItemSellerIdAndStatusIn(seller.getId(), statues, pageable);

    }

    public Page<Auction> getSellingAuctionsBySeller(User seller, Pageable pageable) {
        Page<Auction> auctionPage = auctionRepository.findByItemSellerIdAndStatus(seller.getId(), AppConstants.AUCTION_STT_ACTIVE, pageable);
        auctionPage.getContent().forEach(this::addTimeLeft);
        return auctionPage;
    }

    public Page<Auction> getEndedAuctionsBySeller(User seller, Pageable pageable) {
        return auctionRepository.findByItemSellerIdAndStatus(seller.getId(), AppConstants.AUCTION_STT_ENDED, pageable);
    }

    public Page<Auction> getLostAuctionsByBidder(User bidder, Pageable pageable) {
        return auctionRepository.findLostAuctionsByBidderId(bidder.getId(), AppConstants.AUCTION_STT_ENDED, pageable);
    }

    public Page<Auction> getWonAuctionsByBidder(User bidder, Pageable pageable) {
        return auctionRepository.findByCurrentHighestBidBidderIdAndStatus(bidder.getId(), AppConstants.AUCTION_STT_ENDED, pageable);
    }

    public Page<Auction> getBiddingAuctionsByBidder(User bidder, Pageable pageable) {
        Page<Auction> auctionPage = auctionRepository.findDistinctByBidsBidderIdAndStatus(bidder.getId(), AppConstants.AUCTION_STT_ACTIVE, pageable);
        auctionPage.getContent().forEach(this::addTimeLeft);
        return auctionPage;
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    public void saveBrowseHistory(User user, Integer auctionId) throws AuctionNotFoundException {
        Auction auction = getActiveAuctionById(auctionId);
        if (auctionId == null) {
            throw new AuctionNotFoundException();
        }

        Browse browse = browseRepository.findByUserIdAndAuctionId(user.getId(), auctionId);
        if (browse == null) {
            browse = new Browse();
            browse.setUser(user);
            browse.setAuction(auction);
        }
        browse.setLastBrowsedDate(ZonedDateTime.now());
        browseRepository.save(browse);
    }

    public List<Auction> getRecommendedAuctions(User user, Integer auctionId) throws AuctionNotFoundException {
        if (auctionId == null) {
            throw new AuctionNotFoundException();
        }

        long start = System.currentTimeMillis();
        List<Browse> browses = browseRepository.findAllByAuctionStatus(AppConstants.AUCTION_STT_ACTIVE);
        long end = System.currentTimeMillis();

        long delay = end - start;
        System.out.println(delay);

        List<User> allBrowsingUsers = browses.stream()
                .map(Browse::getUser)
                .filter(distinctByKey(User::getId))
                .collect(Collectors.toList());

        int allBrowsingUsersCount = allBrowsingUsers.size();

        List<Auction> allBrowsedAuctions = browses.stream()
                .map(Browse::getAuction)
                .filter(distinctByKey(Auction::getId))
                .collect(Collectors.toList());

        List<AuctionAffinity> affinities = new ArrayList<>();

        for (int i = 0; i < allBrowsedAuctions.size(); i++) {
            Auction firstAuction = allBrowsedAuctions.get(i);

            List<User> firstBrowsingUsers = browses.stream()
                    .filter(browse -> browse.getAuction().getId().equals(firstAuction.getId()))
                    .map(Browse::getUser)
                    .collect(Collectors.toList());

            for (int j = i; j < allBrowsedAuctions.size(); j++) {
                Auction secondAuction = allBrowsedAuctions.get(j);
                if (secondAuction.getId().equals(firstAuction.getId())) {
                    continue;
                }

                List<User> secondBrowsingUsers = browses.stream()
                        .filter(browse -> browse.getAuction().getId().equals(secondAuction.getId()))
                        .map(Browse::getUser)
                        .collect(Collectors.toList());

                List<User> commonUsers = new ArrayList<>(firstBrowsingUsers);
                commonUsers.retainAll(secondBrowsingUsers);

                Double score = (double) commonUsers.size() / allBrowsingUsersCount;
                AuctionAffinity affinity = new AuctionAffinity(firstAuction, secondAuction, score);
                affinities.add(affinity);
            }
        }

        Collections.sort(affinities);
        Collections.reverse(affinities);

        List<Auction> alreadyViewedAuctions = null;
        if (user != null) {
            alreadyViewedAuctions = browses.stream()
                    .filter(browse -> browse.getUser().getId().equals(user.getId()))
                    .map(Browse::getAuction)
                    .collect(Collectors.toList());
        }

        List<Auction> finalAlreadyViewedAuctions = alreadyViewedAuctions;
        return affinities.stream()
                .map(auctionAffinity -> {
                    if (auctionAffinity.getSecond().getId().equals(auctionId)) {
                        return new AuctionAffinity(auctionAffinity.getSecond(), auctionAffinity.getFirst(), auctionAffinity.getScore());
                    }
                    return auctionAffinity;
                })
                .filter(affinity -> affinity.getFirst().getId().equals(auctionId) && affinity.getScore() > 0)
                .map(auctionAffinity -> {
                    Auction auction = auctionAffinity.getSecond();
                    addTimeLeft(auction);
                    return auction;
                }).filter(auction -> {
                    if (!CollectionUtils.isEmpty(finalAlreadyViewedAuctions)) {
                        return !finalAlreadyViewedAuctions.contains(auction);
                    }
                    return true;
                })
                .collect(Collectors.toList());
    }

    public Page<Auction> getPendingAuctionsBySeller(User seller, Pageable pageable) {
        return auctionRepository.findByItemSellerIdAndStatus(seller.getId(), AppConstants.AUCTION_STT_PENDING_FOR_ACCEPTING, pageable);
    }

    public Page<Auction> getAllActiveAuctions(Pageable pageable) {
        Page<Auction> auctionPage = auctionRepository.findAllByStatus(AppConstants.AUCTION_STT_ACTIVE, pageable);
        auctionPage.getContent().forEach(this::addTimeLeft);
        return auctionPage;
    }

    public List<Auction> getRecentlyViewedAuctions(User user) {
        List<Auction> auctions = browseRepository.findTop10ByUserIdAndAuctionStatusOrderByLastBrowsedDateDesc(user.getId(), AppConstants.AUCTION_STT_ACTIVE)
                .stream()
                .map(Browse::getAuction)
                .collect(Collectors.toList());
        auctions.forEach(this::addTimeLeft);
        return auctions;
    }

    public Integer getCountAllAuctionsByStatus(String status) {
        return auctionRepository.countAllByStatus(status);
    }

    public ChartResponse getDailyCountStartedByChart() {

        List<Integer> counts = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        ZonedDateTime from = DateTimeUtils.getStartOfToDay();
        ZonedDateTime to = DateTimeUtils.getEndOfToDay();
        for (int i = AppConstants.MAX_SIZE_DATASET_DAILY; i > 0; i--) {
            int count = getCountAuctionsByCreatedDateBetween(from, to);
            String label = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(to);
            counts.add(count);
            labels.add(label);
            from = from.minusDays(1);
            to = to.minusDays(1);
        }

        ChartResponse response = new ChartResponse();
        response.setDataSet(counts);
        response.setLabels(labels);
        return response;
    }

    public ChartResponse getWeeklyCountStartByChart() {
        List<Integer> counts = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        ZonedDateTime from = DateTimeUtils.getStartOfDateTime(ZonedDateTime.now().with(TemporalAdjusters.previous(DayOfWeek.MONDAY)));
        ZonedDateTime to = DateTimeUtils.getEndOfDateTime(from.plusDays(6));

        for (int i = AppConstants.MAX_SIZE_DATASET_WEEKLY; i > 0; i--) {
            int count = getCountAuctionsByCreatedDateBetween(from, to);
            String label = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(from);
            counts.add(count);
            labels.add(label);
            from = from.minusDays(7);
            to = to.minusDays(7);
        }

        ChartResponse response = new ChartResponse();
        response.setDataSet(counts);
        response.setLabels(labels);
        return response;
    }

    public ChartResponse getMonthlyCountStartByChart() {
        List<Integer> counts = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        ZonedDateTime from = DateTimeUtils.getStartOfDateTime(ZonedDateTime.now().with(TemporalAdjusters.firstDayOfMonth()));
        ZonedDateTime to = DateTimeUtils.getEndOfDateTime(from.with(TemporalAdjusters.lastDayOfMonth()));

        for (int i = AppConstants.MAX_SIZE_DATASET_MONTHLY; i > 0; i--) {
            int count = getCountAuctionsByCreatedDateBetween(from, to);
            String label = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(from);
            counts.add(count);
            labels.add(label);

            to = DateTimeUtils.getEndOfDateTime(from.minusDays(1));
            from = DateTimeUtils.getStartOfDateTime(to.with(TemporalAdjusters.firstDayOfMonth()));
        }

        ChartResponse response = new ChartResponse();
        response.setDataSet(counts);
        response.setLabels(labels);
        return response;
    }

    private Integer getCountAuctionsByCreatedDateBetween(ZonedDateTime from, ZonedDateTime to) {
        return auctionRepository.countAllByStartTimeBetween(from, to);
    }

    public List<AuctionStatistics> getTop5AuctionsHaveMostBids() {
        return auctionRepository.findTop5AuctionsHaveMostBids();
    }

    public Page<Auction> getAuctionByStatus(String status, Pageable pageable) {
        return auctionRepository.findAllByStatus(status, pageable);
    }

    public Page<Auction> getAuctionByNameAndStatus(String name, String status, Pageable pageable) {
        return auctionRepository.findByItemNameContainingAndStatus(name, status, pageable);
    }
}