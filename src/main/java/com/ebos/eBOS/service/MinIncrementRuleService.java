package com.ebos.eBOS.service;

import com.ebos.eBOS.data.entity.MinIncrementRule;
import com.ebos.eBOS.repository.MinIncrementRuleRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MinIncrementRuleService {

    private MinIncrementRuleRepository minIncrementRuleRepository;

    public MinIncrementRuleService(MinIncrementRuleRepository minIncrementRuleRepository) {
        this.minIncrementRuleRepository = minIncrementRuleRepository;
    }

    public List<MinIncrementRule> getAll() {
        return minIncrementRuleRepository.findAll();
    }

}
