package com.ebos.eBOS.service;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.UserStatistics;
import com.ebos.eBOS.data.entity.PersonalMember;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.exception.ResourceNotFoundException;
import com.ebos.eBOS.repository.PersonalMemberRepository;
import com.ebos.eBOS.repository.RoleRepository;
import com.ebos.eBOS.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;

@Service
public class UserService {

    private static final int ADMIN_ID = 1;
    private static final int AUCTIONEER_ID = 2;
    private static final int BIDDER_ID = 3;

    private UserRepository userRepository;
    private PersonalMemberRepository personalMemberRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       PersonalMemberRepository personalMemberRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.personalMemberRepository = personalMemberRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User findByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }

    public boolean checkExistingUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public boolean checkExistingPhone(String phone) {
        return userRepository.existsByPhone(phone);
    }

    public boolean checkExistingEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public void insertUser(User user) {
        user.setCreatedDate(ZonedDateTime.now());
        userRepository.insert(user.getUsername(), user.getPassword(), user.getPhone(), user.getEmail(), AppConstants.USER_STT_ACTIVE,
                user.getCreatedDate());
    }

    public void personalSignUp(PersonalMember member) {
        User user = member.getUser();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setStatus(AppConstants.USER_STT_ACTIVE);

        insertUser(user);
        user = findByPhone(user.getPhone());

        member.setId(user.getId());
        personalMemberRepository.insert(member.getId(), member.getFullName(), member.getDateOfBirth());

        roleRepository.saveByUserId(user.getId(), AUCTIONEER_ID);
        roleRepository.saveByUserId(user.getId(), BIDDER_ID);
    }

    public Page<PersonalMember> getPersonalMembers(Pageable pageRequest) {
        return personalMemberRepository.findAll(pageRequest);
    }

    public PersonalMember getPersonalMemberById(int id) {
        return personalMemberRepository.findById(id).orElse(null);
    }

    public boolean checkExistsById(int id) {
        return userRepository.existsById(id);
    }

    public List<UserStatistics> getTop5SellersHavingMostAuctions() {
        return userRepository.findTop5SellersHaveMostAuctions();
    }

    public List<UserStatistics> getTop5BiddersHaveMostWonItems() {
        return userRepository.findTop5BiddersHaveMostWonItems();
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public Page<PersonalMember> getPersonalMemberByStatus(String status, Pageable pageable) {
        return personalMemberRepository.findPersonalMemberByUser_Status(status, pageable);
    }

    public void activatePersonalMember(Integer id, User admin) throws ResourceNotFoundException {
        PersonalMember personalMember = personalMemberRepository.findById(id).orElse(null);
        if (personalMember == null) {
            throw new ResourceNotFoundException(AppConstants.EXCEPTION_MEMBER_NOT_FOUND);
        }
        User user = personalMember.getUser();
        user.setLastModifiedDate(ZonedDateTime.now());
        user.setLastModifiedBy(admin.getId());
        activateUser(user);
    }

    public void restrictPersonalMember(Integer id, String note, User admin) throws ResourceNotFoundException {
        PersonalMember personalMember = personalMemberRepository.findById(id).orElse(null);
        if (personalMember == null) {
            throw new ResourceNotFoundException(AppConstants.EXCEPTION_MEMBER_NOT_FOUND);
        }
        User user = personalMember.getUser();
        user.setNote(note);
        user.setLastModifiedDate(ZonedDateTime.now());
        user.setLastModifiedBy(admin.getId());
        restrictUser(personalMember.getUser());
    }

    public void activateUser(User user) {
        user.setStatus(AppConstants.USER_STT_ACTIVE);
        userRepository.save(user);
    }

    public void restrictUser(User user) {
        user.setStatus(AppConstants.USER_STT_RESTRICTED);
        userRepository.save(user);
    }

    public Integer countPersonalMembersByStatus(String status) {
        return personalMemberRepository.countAllByUserStatus(status);
    }

    public Page<PersonalMember> getPersonalMembersByKeyword(String keyword, String status, Pageable pageable) {
        keyword = keyword.toLowerCase();
        return personalMemberRepository.findAllPersonalMembersByKeyWord(keyword, status, pageable);
    }

    public String getStatus(Integer id) throws ResourceNotFoundException {
        User user = userRepository.findStatusById(id);
        if (user == null) {
            throw new ResourceNotFoundException(AppConstants.EXCEPTION_ACCOUNT_NOT_FOUND);
        }
        return user.getStatus();
    }
}

