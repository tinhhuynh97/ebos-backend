package com.ebos.eBOS.service;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.entity.*;
import com.ebos.eBOS.exception.AuctionNotFoundException;
import com.ebos.eBOS.exception.InvalidRequestException;
import com.ebos.eBOS.exception.ResourceNotFoundException;
import com.ebos.eBOS.repository.AddressRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class AddressService {

    private AddressRepository addressRepository;
    private AuctionService auctionService;
    private UserService userService;


    public AddressService(AddressRepository addressRepository, UserService userService, AuctionService auctionService, ItemService itemService) {
        this.addressRepository = addressRepository;
        this.userService = userService;
        this.auctionService = auctionService;
    }

    public void insertPersonalAddress(Address address) {
        User owner = address.getOwner();
        if (address.getIsShippingPrimary()) {
            Address currentShippingPrimaryAddr = addressRepository.findByOwnerIdAndIsShippingPrimaryTrue(owner.getId());
            if (currentShippingPrimaryAddr != null) {
                currentShippingPrimaryAddr.setIsShippingPrimary(false);
                addressRepository.save(currentShippingPrimaryAddr);
            }
        }
        if (address.getIsShipPrimary()) {
            Address currentShipPrimaryAddr = addressRepository.findByOwnerIdAndIsShipPrimaryTrue(owner.getId());
            if (currentShipPrimaryAddr != null) {
                currentShipPrimaryAddr.setIsShipPrimary(false);
                addressRepository.save(currentShipPrimaryAddr);
            }
        }
        address.setIsDeleted(false);
        addressRepository.save(address);
    }

    public boolean checkHavingAnyAddress(User user) {
        return addressRepository.existsByOwnerIdAndIsDeletedFalse(user.getId());
    }

    public Address getSellerContactInfoOfWonItem(User bidder, Integer auctionId) throws ResourceNotFoundException, InvalidRequestException {
        Auction auction = auctionService.getEndedAuctionById(auctionId);
        if (auction == null) {
            throw new AuctionNotFoundException();
        }
        if (!auction.getCurrentHighestBid().getBidder().getId().equals(bidder.getId())) {
            throw new InvalidRequestException(AppConstants.EXCEPTION_NO_PERMISSION_TO_CONTACT_SELLER);
        }

        PersonalMember seller = userService.getPersonalMemberById(auction.getItem().getSeller().getId());

        Address address = addressRepository.findByOwnerIdAndIsShipPrimaryTrue(seller.getId());

        if (StringUtils.isEmpty(address.getName())) {
            address.setName(seller.getFullName());
        }
        if (StringUtils.isEmpty(address.getPhone())) {
            address.setPhone(seller.getUser().getPhone());
        }
        return address;
    }

    public Address getItemWinnerContactInfo(User seller, Integer auctionId) throws InvalidRequestException, ResourceNotFoundException {
        Auction auction = auctionService.getEndedAuctionById(auctionId);
        if (auction == null) {
            throw new AuctionNotFoundException();
        }
        if (!auction.getItem().getSeller().getId().equals(seller.getId())) {
            throw new InvalidRequestException(AppConstants.EXCEPTION_NO_PERMISSION_TO_CONTACT_WINNER);
        }

        Bid highestBid = auction.getCurrentHighestBid();

        if (highestBid == null) {
            throw new InvalidRequestException(AppConstants.EXCEPTION_ITEM_HAS_NO_WINNER);
        }

        PersonalMember bidder = userService.getPersonalMemberById(highestBid.getBidder().getId());

        Address address = addressRepository.findByOwnerIdAndIsShippingPrimaryTrue(bidder.getId());

        if (StringUtils.isEmpty(address.getName())) {
            address.setName(bidder.getFullName());
        }
        if (StringUtils.isEmpty(address.getPhone())) {
            address.setPhone(bidder.getUser().getPhone());
        }
        return address;
    }
}
