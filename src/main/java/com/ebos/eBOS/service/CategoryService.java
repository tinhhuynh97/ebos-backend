package com.ebos.eBOS.service;

import com.ebos.eBOS.data.CategoryStatistics;
import com.ebos.eBOS.data.entity.Category;
import com.ebos.eBOS.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    private CategoryRepository categoryRepository;


    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> getCategories() {
        List<Category> categories = categoryRepository.findAll();
        categories.forEach(category -> {
            int numOfChildren = getNumberOfChilden(category.getId(), categories);
            category.setNumOfChildren(numOfChildren);
        });
        return categories;
    }

    public Category getById(Integer categoryId) {
        return categoryRepository.findById(categoryId).orElse(null);
    }

    public List<CategoryStatistics> getTop5CategoriesHaveMostItems() {
        return categoryRepository.findTop5CategoriesHaveMostItems();
    }

    private int getNumberOfChilden(int categoryId, List<Category> categories) {
        return categories.stream()
                .filter(category -> category.getParentId() != null && category.getParentId().equals(categoryId))
                .collect(Collectors.toList()).size();
    }

}
