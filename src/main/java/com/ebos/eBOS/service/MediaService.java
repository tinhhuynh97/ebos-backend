package com.ebos.eBOS.service;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.entity.Item;
import com.ebos.eBOS.data.entity.Media;
import com.ebos.eBOS.repository.MediaRepository;
import com.ebos.eBOS.service.cloudinary.CloudinaryService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

@Service
public class MediaService {

    private MediaRepository mediaRepository;
    private CloudinaryService cloudinaryService;

    public MediaService(MediaRepository mediaRepository, CloudinaryService cloudinaryService) {
        this.mediaRepository = mediaRepository;
        this.cloudinaryService = cloudinaryService;
    }

    public void addAll(Item item, List<MultipartFile> images) {
        images.forEach(image -> {
            try {
                Map result = cloudinaryService.upload(image);
                String url = (String) result.get("secure_url");
                Media media = new Media();
                media.setUrl(url);
                media.setItem(item);
                media.setType(AppConstants.MEDIA_TYPE_IMAGE);
                media.setIsDeleted(false);
                media.setCreatedDate(ZonedDateTime.now());
                mediaRepository.save(media);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
