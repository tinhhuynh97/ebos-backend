package com.ebos.eBOS.service.quartz.job;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.entity.Auction;
import com.ebos.eBOS.data.firebase.db.FbAuction;
import com.ebos.eBOS.service.AuctionService;
import com.ebos.eBOS.service.firebase.DatabaseService;
import com.ebos.eBOS.service.firebase.FcmService;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.time.ZonedDateTime;

@Component
public class EndAuctionJob implements Job {

    private AuctionService auctionService;
    private DatabaseService databaseService;
    private FcmService fcmService;


    public EndAuctionJob() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        int auctionId = jobDataMap.getIntegerFromString(AppConstants.JMD_AUCTION_ID);

        Auction auction = auctionService.getActiveAuctionById(auctionId);
        if (auction != null) {
            auction.setStatus(AppConstants.AUCTION_STT_ENDED);
            auction.setActualEndTime(ZonedDateTime.now());
            auctionService.save(auction);
            databaseService.save(FbAuction.from(auction));
            fcmService.sendMessageToUsersFromAuction(auction);
        }
    }

    @Autowired
    public void setAuctionService(AuctionService auctionService) {
        this.auctionService = auctionService;
    }

    @Autowired
    public void setDatabaseService(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    @Autowired
    public void setFcmService(FcmService fcmService) {
        this.fcmService = fcmService;
    }

}
