package com.ebos.eBOS.service.quartz.job;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.service.firebase.FcmService;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.util.HashMap;
import java.util.Map;

@Component
public class NotificationJob implements Job {

    private FcmService fcmService;

    public NotificationJob() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        String title = jobDataMap.getString(AppConstants.JMD_NOTIFICATION_TITLE);
        String body = jobDataMap.getString(AppConstants.JMD_NOTIFICATION_BODY);
        int userId = jobDataMap.getIntegerFromString(AppConstants.JMD_NOTIFICATION_USER_ID);

        Map<String, String> data = new HashMap<>();

        jobDataMap.forEach((key, value) -> {
            if (key.contains("NOTIFICATION_DATA")) {
                data.put(key, value.toString());
            }
        });
        fcmService.sendMessageToUser(userId, title, body, data);
    }

    @Autowired
    public void setFcmService(FcmService fcmService) {
        this.fcmService = fcmService;
    }
}
