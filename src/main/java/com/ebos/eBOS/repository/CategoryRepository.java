package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.CategoryStatistics;
import com.ebos.eBOS.data.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface CategoryRepository extends JpaRepository<Category, Integer> {

    @Query(value = "SELECT C.name AS category, COUNT(A.id) AS count FROM Category C " +
            "JOIN Item I on C.id = I.categoryId " +
            "JOIN Auction A on I.id = A.itemId " +
            "GROUP BY C.id " +
            "ORDER BY count DESC " +
            "LIMIT 5;", nativeQuery = true)
    List<CategoryStatistics> findTop5CategoriesHaveMostItems();

}
