package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    @Query(value = "SELECT * FROM Role " +
            "JOIN UserRole " +
            "ON Role.id = UserRole.roleId " +
            "WHERE UserRole.userId = ?1", nativeQuery = true)
    List<Role> findByUserId(Integer userId);

    @Modifying
    @Query(value = "INSERT INTO UserRole (userId,roleId) " +
            "VALUES (?1, ?2)", nativeQuery = true)
    @Transactional
    void saveByUserId(Integer userId, Integer roleId);

    Role findByName(String name);
}
