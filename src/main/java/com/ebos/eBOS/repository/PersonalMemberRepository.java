package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.PersonalMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.sql.Timestamp;

public interface PersonalMemberRepository extends JpaRepository<PersonalMember, Integer> {
    @Modifying
    @Query(value = "INSERT INTO PersonalMember (id, fullName, dateOfBirth) " +
            "VALUES (?1, ?2, ?3)", nativeQuery = true)
    @Transactional
    void insert(Integer id, String fullName, Timestamp dateOfBirth);

    Page<PersonalMember> findPersonalMemberByUser_Status(String status, Pageable pageable);

    Integer countAllByUserStatus(String status);

    @Query(value = "SELECT p FROM PersonalMember p " +
            "JOIN p.user u " +
            "WHERE (u.username LIKE CONCAT('%' ,?1, '%') " +
            "OR u.phone LIKE CONCAT('%' ,?1, '%') " +
            "OR p.fullName LIKE CONCAT('%' ,?1, '%')) " +
            "AND u.status = ?2")
    Page<PersonalMember> findAllPersonalMembersByKeyWord(String keyword, String status, Pageable pageable);
}
