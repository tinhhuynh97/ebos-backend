package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.MinIncrementRule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MinIncrementRuleRepository extends JpaRepository<MinIncrementRule, Integer> {



}
