package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.Media;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaRepository extends JpaRepository<Media, Integer> {
}
