package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.UserStatistics;
import com.ebos.eBOS.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByPhone(String phone);

    boolean existsById(int id);

    boolean existsByEmail(String email);

    boolean existsByPhone(String phone);

    boolean existsByUsername(String username);

    @Modifying
    @Query(value = "INSERT INTO User (username, password, phone, email, status, createdDate) " +
            "VALUES (?1, ?2, ?3, ?4, ?5, ?6)", nativeQuery = true)
    @Transactional
    void insert(String username, String password, String phone, String email, String stt, ZonedDateTime createdDate);

    @Query(value = "SELECT COUNT(DISTINCT u.id) FROM User u " +
            "WHERE EXISTS (SELECT 1 FROM Browse b WHERE b.user = u)")
    Integer countAllBrowsingUsers();

    @Query(value = "SELECT u.username AS username, COUNT(a.id) AS count FROM User u " +
            "JOIN Item i on u.id = i.sellerId " +
            "JOIN Auction a on i.id = a.itemId " +
            "WHERE a.status = 'ACTIVE' OR a.status = 'ENDED' " +
            "GROUP BY u.id " +
            "ORDER BY count DESC " +
            "LIMIT 5", nativeQuery = true)
    List<UserStatistics> findTop5SellersHaveMostAuctions();

    @Query(value = "SELECT u.username AS username, COUNT(b.id) AS count FROM User u " +
            "JOIN Bid b on u.id = b.bidderId " +
            "JOIN Auction a on b.auctionId = a.id " +
            "WHERE a.status = 'ENDED' AND a.highestBidId = b.id " +
            "group by u.id " +
            "ORDER BY count DESC " +
            "LIMIT 5", nativeQuery = true)
    List<UserStatistics> findTop5BiddersHaveMostWonItems();

    @Query(value = "SELECT u.id, u.status FROM User u WHERE u.id = ?1")
    User findStatusById(Integer id);

}
