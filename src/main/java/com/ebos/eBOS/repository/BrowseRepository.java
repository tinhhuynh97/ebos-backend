package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.Browse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BrowseRepository extends JpaRepository<Browse, Integer> {

    Browse findByUserIdAndAuctionId(Integer userId, Integer auctionId);

    List<Browse> findAllByAuctionStatus(String status);

    List<Browse> findAllByUserIdAndAuctionStatus(Integer userId, String status);

    List<Browse> findTop10ByUserIdAndAuctionStatusOrderByLastBrowsedDateDesc(Integer userId, String status);

    List<Browse> findAllByAuctionId(Integer auctionId);

    List<Browse> findAllByUserIdNotAndAuctionStatus(Integer userId, String status);

}
