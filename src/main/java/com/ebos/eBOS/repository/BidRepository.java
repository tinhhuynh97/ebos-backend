package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.Bid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.ZonedDateTime;

public interface BidRepository extends JpaRepository<Bid, Integer> {

    @Query(value = "select COUNT(b.id) from Bid b " +
            "join b.bidder u " +
            "join b.auction a " +
            "where u.id= ?1  and a.id = ?2")
    Integer countByBidderIdAndAuctionId(Integer bidderId, Integer auctionId);

    Integer countAllByCreatedDateBetweenAndIsAutoFalse(ZonedDateTime from, ZonedDateTime to);
}
