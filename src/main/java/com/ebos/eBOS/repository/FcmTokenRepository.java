package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.FcmToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FcmTokenRepository extends JpaRepository<FcmToken, Integer> {
    Boolean existsByUserIdAndValue(Integer userId, String value);

    FcmToken findByUserIdAndValue(Integer userId, String value);

    FcmToken findByValue(String value);

    List<FcmToken> findByUserId(Integer userId);
}
