package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.AuctionStatistics;
import com.ebos.eBOS.data.entity.Auction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

public interface AuctionRepository extends JpaRepository<Auction, Integer>, JpaSpecificationExecutor<Auction> {

    Page<Auction> findAllByStatusOrderByCreatedDateAsc(String status, Pageable pageable);

    @Query(value = "SELECT a FROM Auction a " +
            "JOIN a.item i " +
            "JOIN i.category c " +
            "WHERE c.id = ?1 AND a.status = ?2 AND a.currentPrice BETWEEN ?3 AND ?4")
    Page<Auction> findAllByCategoryIdAndStatus(Integer categoryId, String status, BigDecimal minPrice, BigDecimal maxPrice, Pageable pageable);

    @Query(value = "SELECT a FROM Auction a " +
            "JOIN a.item i " +
            "WHERE ((MATCH(i.name, ?1) > 0 OR i.name LIKE CONCAT('%' ,?1, '%')) " +
            "OR (MATCH(i.description, ?1) > 0 OR i.description LIKE CONCAT('%' ,?1, '%'))) " +
            "AND a.status = ?2 AND a.currentPrice BETWEEN ?3 AND ?4")
    Page<Auction> findAllByKeywordAndStatus(String keyword, String status, BigDecimal minPrice, BigDecimal maxPrice, Pageable pageable);

    @Query(value = "SELECT * FROM Auction " +
            "JOIN Item ON Item.id = Auction.itemId " +
            "WHERE Auction.status = ?1 " +
            "ORDER BY Auction.startTime DESC", nativeQuery = true)
    Page<Auction> findAllByNewlyListedAndStatus(String auctionSttActive, Pageable pageable);

    @Query(value = "SELECT * FROM Auction " +
            "JOIN Item ON Item.id = Auction.itemId " +
            "WHERE Auction.status = ?1 " +
            "ORDER BY (TIMEDIFF(NOW(), DATE_ADD(Auction.startTime, INTERVAL Auction.durationInDay DAY ))) DESC", nativeQuery = true)
    Page<Auction> findAllByEndingSoonAndStatus(String auctionSttActive, Pageable pageable);

    Auction findByIdAndStatus(Integer id, String status);

    List<Auction> findAllByStatus(String status);

    Page<Auction> findAllByStatus(String status, Pageable pageable);

    Page<Auction> findByItemSellerIdAndStatus(Integer integer, String status, Pageable pageable);

    Page<Auction> findDistinctByItemSellerIdAndStatusIn(Integer sellerId, List<String> statuses, Pageable pageable);

    @Query(value = "SELECT a FROM Auction a " +
            "JOIN a.bids bs " +
            "JOIN a.currentHighestBid hb " +
            "WHERE a.status = ?2 AND bs.bidder.id = ?1 " +
            "AND hb.bidder.id <> bs.bidder.id " +
            "GROUP BY a.id")
    Page<Auction> findLostAuctionsByBidderId(Integer bidderId, String status, Pageable pageable);

    //    @Query(value = "SELECT a FROM Auction a " +
//            "WHERE a.status = ?2 AND a.currentHighestBid.bidder.id = ?1")
    Page<Auction> findByCurrentHighestBidBidderIdAndStatus(Integer bidderId, String status, Pageable pageable);

    @Query(value = "SELECT a FROM Auction a " +
            "JOIN a.bids b " +
            "WHERE a.status = ?2 " +
            "AND b.bidder.id = ?1 " +
            "GROUP BY a.id")
    Page<Auction> findDistinctByBidsBidderIdAndStatus(Integer bidderId, String status, Pageable pageable);

    @Query(value = "SELECT DISTINCT a FROM Auction a " +
            "WHERE EXISTS (SELECT 1 FROM Browse b WHERE b.auction = a) AND a.status = ?1")
    List<Auction> findAllBrowsedAuctionByStatus(String status);

    Integer countAllByStatus(String status);

    Integer countAllByStartTimeBetween(ZonedDateTime from, ZonedDateTime to);

    @Query(value = "SELECT I.name AS itemName, IFNULL(A.numberOfBids, 0) AS count FROM Auction A " +
            "  JOIN Item I on A.itemId = I.id " +
            "  WHERE A.status = 'ACTIVE' " +
            "  GROUP BY A.id " +
            "  ORDER BY count DESC LIMIT 5", nativeQuery = true)
    List<AuctionStatistics> findTop5AuctionsHaveMostBids();

    Page<Auction> findByItemNameContainingAndStatus(String name, String status, Pageable pageable);
}
