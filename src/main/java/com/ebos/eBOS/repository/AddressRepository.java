package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {

    Address findByOwnerIdAndIsShippingPrimaryTrue(Integer ownerId);

    Address findByOwnerIdAndIsShipPrimaryTrue(Integer ownerId);

    boolean existsByOwnerIdAndIsDeletedFalse(Integer ownerId);

}
