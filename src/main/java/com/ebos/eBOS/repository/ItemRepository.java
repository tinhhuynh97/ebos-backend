package com.ebos.eBOS.repository;

import com.ebos.eBOS.data.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Integer> {

}
