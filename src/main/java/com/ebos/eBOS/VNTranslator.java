package com.ebos.eBOS;

import java.util.HashMap;
import java.util.Map;

public class VNTranslator {

    private static VNTranslator INSTANCE = null;

    private Map<String, String> data;

    private VNTranslator() {
        data = new HashMap<>();
        data.put(AppConstants.REJECT_REASON_DUPLICATED_ITEM, "Sản phẩm trùng lặp");
        data.put(AppConstants.REJECT_REASON_INAPPROPRIATE_IMAGES, "Hình ảnh ko phù hợp, ảnh lấy từ internet, chỉnh sửa quá nhiều,...");
        data.put(AppConstants.REJECT_REASON_INAPPROPRIATE_INFO, "Thông tin sản phầm ko phù hợp, thiếu mô tả chi tiết");
        data.put(AppConstants.REJECT_REASON_PROHIBITED_RESTRICTED_ITEM, "Sản phẩm ko thuộc phạm vi, ko hợp pháp, bị cấm,...");
    }

    public static VNTranslator getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new VNTranslator();
        }
        return INSTANCE;
    }

    public String get(String key) {
        return data.get(key);
    }

}
