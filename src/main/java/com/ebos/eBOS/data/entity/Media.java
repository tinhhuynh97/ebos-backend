package com.ebos.eBOS.data.entity;

import com.ebos.eBOS.ZonedDateTimeAttributeConverter;
import com.ebos.eBOS.data.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
public class Media {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Simple.class)
    private Integer id;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "itemID", referencedColumnName = "id")
    private Item item;
    @JsonView(Views.Simple.class)
    private String url;
    private String type;
    @JsonIgnore
    private Boolean isDeleted;
    @JsonIgnore
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime createdDate;

}
