package com.ebos.eBOS.data.entity;

import com.ebos.eBOS.ZonedDateTimeAttributeConverter;
import com.ebos.eBOS.data.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Simple.class)
    private Integer id;
    @JsonView(Views.Simple.class)
    private String name;
    @JsonView(Views.Simple.class)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "categoryId", referencedColumnName = "id")
    private Category category;
    @Column(name = "`condition`")
    @JsonView(Views.Details.class)
    private String condition;
    @JsonView(Views.Details.class)
    private String description;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sellerId")
    @JsonView({Views.Details.class, Views.BidderHistory.class})
    private User seller;
    @JsonIgnore
    private Boolean isDeleted;
    @JsonIgnore
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime createdDate;
    @JsonView(Views.Simple.class)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "item")
    @Fetch(FetchMode.SUBSELECT)
    private List<Media> media;
}
