package com.ebos.eBOS.data.entity;

import com.ebos.eBOS.ZonedDateTimeAttributeConverter;
import com.ebos.eBOS.data.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.PublicInfo.class)
    private Integer id;
    @JsonView(Views.PublicInfo.class)
    private String username;
    @JsonView(Views.PrivateInfo.class)
    private String phone;
    @JsonView(Views.PrivateInfo.class)
    private String email;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @JsonView(Views.PrivateInfo.class)
    private String status;
    @JsonView(Views.PrivateInfo.class)
    private String note;
    @CreatedDate
    @JsonIgnore
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime createdDate;
    @LastModifiedDate
    @JsonIgnore
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime lastModifiedDate;
    @JsonIgnore
    private Integer lastModifiedBy;
    @Transient
    @JsonIgnore
    private List<Role> roles;

}
