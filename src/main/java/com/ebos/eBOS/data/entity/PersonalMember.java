package com.ebos.eBOS.data.entity;

import com.ebos.eBOS.data.Views;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
public class PersonalMember implements Member {
    @Id
    @JsonView(Views.PublicInfo.class)
    private Integer id;
    private String fullName;
    @JsonView(Views.PrivateInfo.class)
    private Timestamp dateOfBirth;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id", name = "id")
    @JsonView(Views.PublicInfo.class)
    private User user;
//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner")
//    @JsonIgnore
//    private List<Address> addresses;
}
