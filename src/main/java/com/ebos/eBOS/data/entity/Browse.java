package com.ebos.eBOS.data.entity;

import com.ebos.eBOS.ZonedDateTimeAttributeConverter;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
public class Browse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "auctionId", referencedColumnName = "id")
    private Auction auction;
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime lastBrowsedDate;
}
