package com.ebos.eBOS.data.entity;

import com.ebos.eBOS.ZonedDateTimeAttributeConverter;
import com.ebos.eBOS.data.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Auction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Simple.class)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "itemId", referencedColumnName = "id")
    @JsonView(Views.Simple.class)
    private Item item;
    @JsonView(Views.Simple.class)
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime startTime;
    @JsonView(Views.Simple.class)
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime actualEndTime;
    @JsonView(Views.Simple.class)
    private Integer durationInDay;
    @JsonView(Views.Simple.class)
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime plannedEndTime;
    @JsonView(Views.Simple.class)
    private BigDecimal reservePrice;
    @JsonView(Views.Simple.class)
    private BigDecimal currentPrice;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id", name = "highestBidId")
    @JsonView({Views.Details.class, Views.History.class})
    private Bid currentHighestBid;
    @JsonIgnore
    private BigDecimal currentMinimumBidForHighestBidder;
    @JsonView({Views.Details.class, Views.History.class})
    private BigDecimal currentMinimumBid;
    @JsonView(Views.Simple.class)
    private Integer numberOfBids;
    @JsonView(Views.Simple.class)
    @Transient
    private Long timeLeft;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "auction")
    @Fetch(FetchMode.SUBSELECT)
    @JsonView(Views.Details.class)
    List<Bid> bids;
    @JsonView(Views.Simple.class)
    private String status;
    @JsonView({Views.PrivateInfo.class, Views.AuctioneerHistory.class})
    private String rejectedReason;
    @JsonView({Views.PrivateInfo.class, Views.AuctioneerHistory.class})
    private String note;
    @JsonView(Views.Simple.class)
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime createdDate;
    @JsonView(Views.AuctioneerHistory.class)
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime lastModifiedDate;
    @JsonIgnore
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "lastModifiedBy", referencedColumnName = "id")
    private User lastModifiedBy;
}
