package com.ebos.eBOS.data.entity;

import com.ebos.eBOS.ZonedDateTimeAttributeConverter;
import com.ebos.eBOS.data.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;


@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
public class Bid {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.PublicInfo.class)
    private Integer id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bidderId", referencedColumnName = "id")
    @JsonView(Views.PublicInfo.class)
    private User bidder;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "auctionId", referencedColumnName = "id")
    private Auction auction;
    @JsonView(Views.PublicInfo.class)
    private BigDecimal price;
    @JsonIgnore
    private BigDecimal maxPrice;
    @JsonView(Views.PrivateInfo.class)
    private boolean isAuto;
    @JsonView(Views.PrivateInfo.class)
    private boolean isHidden;
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    @JsonView(Views.PublicInfo.class)
    private ZonedDateTime createdDate;

}
