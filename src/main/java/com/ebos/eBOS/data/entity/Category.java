package com.ebos.eBOS.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
public class Category {
    @Transient
    @JsonIgnore
    List<Category> subcategories;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer parentId;
    @JsonIgnore
    private Boolean isDeleted;
    @Transient
    private Integer numOfChildren;
}
