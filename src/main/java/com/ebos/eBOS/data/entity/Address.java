package com.ebos.eBOS.data.entity;


import com.ebos.eBOS.ZonedDateTimeAttributeConverter;
import com.ebos.eBOS.data.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.PrivateInfo.class)
    private Integer id;
    @JsonView({Views.PrivateInfo.class, Views.ContactInfo.class})
    private String name;
    @JsonView({Views.PrivateInfo.class, Views.ContactInfo.class})
    private String phone;
    @JsonView({Views.PrivateInfo.class, Views.ContactInfo.class})
    private String addressLine1;
    private String addressLine2;
    @JsonView({Views.PrivateInfo.class, Views.ContactInfo.class})
    private String district;
    @JsonView({Views.PrivateInfo.class, Views.ContactInfo.class})
    private String city;
    @JsonView({Views.PrivateInfo.class, Views.ContactInfo.class})
    private String country;
    @JsonView({Views.PrivateInfo.class, Views.ContactInfo.class})
    private String zipCode;
    @JsonView(Views.PrivateInfo.class)
    private Boolean isShippingPrimary;
    @JsonView(Views.PrivateInfo.class)
    private Boolean isShipPrimary;
    @JsonIgnore
    private Boolean isDeleted;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ownerId", referencedColumnName = "id")
    private User owner;
    @JsonIgnore
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime createdDate;

}
