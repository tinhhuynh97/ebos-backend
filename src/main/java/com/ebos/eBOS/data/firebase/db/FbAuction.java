package com.ebos.eBOS.data.firebase.db;

import com.ebos.eBOS.data.entity.Auction;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Getter
@Setter
public class FbAuction {
    private String id = "";
    private String currentPrice = "0";
    private String currentMinimumBid;
    private String currentMinimumBidForHighestBidder;
    private FbBid currentHighestBid;
    private String status = "";
    private Integer numberOfBids = 0;
    private List<FbBid> bids = new ArrayList<>();

    public static FbAuction from(Auction auction) {
        FbAuction fbAuction = new FbAuction();
        fbAuction.id = auction.getId() + "";
        fbAuction.currentPrice = auction.getCurrentPrice() + "";
        fbAuction.currentMinimumBid = auction.getCurrentMinimumBid() + "";
        fbAuction.currentMinimumBidForHighestBidder = auction.getCurrentMinimumBidForHighestBidder() + "";
        fbAuction.numberOfBids = auction.getNumberOfBids();
        if (auction.getCurrentHighestBid() != null) {
            fbAuction.currentHighestBid = FbBid.from(auction.getCurrentHighestBid());
        }
        fbAuction.status = auction.getStatus();
        if (!CollectionUtils.isEmpty(auction.getBids())) {
            fbAuction.bids = auction.getBids()
                    .stream()
                    .filter(bid -> !bid.isHidden())
                    .map(FbBid::from).collect(Collectors.toList());
        }
        return fbAuction;
    }
}
