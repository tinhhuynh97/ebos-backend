package com.ebos.eBOS.data.firebase.db;

import com.ebos.eBOS.data.entity.Bid;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.ZoneOffset;

@Data
@Getter
@Setter
public class FbBid {
    private String id;
    private String bidderUsername = "";
    private String price = "0";
    private String biddingDate = "";
    private boolean isAuto;
    private boolean isHidden;

    public static FbBid from(Bid bid) {
        FbBid fbBid = new FbBid();
        fbBid.id = bid.getId() + "";
        fbBid.bidderUsername = bid.getBidder().getUsername();
        fbBid.price = bid.getPrice() + "";
        fbBid.biddingDate = bid.getCreatedDate().toLocalDateTime().toInstant(ZoneOffset.of("+7")).toEpochMilli() + "";
        fbBid.isAuto = bid.isAuto();
        fbBid.isHidden = bid.isHidden();
        return fbBid;
    }

}