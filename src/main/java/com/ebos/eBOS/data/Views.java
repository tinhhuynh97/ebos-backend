package com.ebos.eBOS.data;

public class Views {
    public interface PublicInfo {
    }

    public interface PrivateInfo extends PublicInfo {
    }

    public interface ContactInfo extends PublicInfo {

    }

    public interface Simple extends PublicInfo {
    }

    public interface Details extends Simple {
    }

    public interface History extends Simple {

    }

    public interface AuctioneerHistory extends History {

    }

    public interface BidderHistory extends History {

    }
}
