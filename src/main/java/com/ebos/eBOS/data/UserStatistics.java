package com.ebos.eBOS.data;


public interface UserStatistics {
    String getUsername();
    int getCount();
}
