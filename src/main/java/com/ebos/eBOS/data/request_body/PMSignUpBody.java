package com.ebos.eBOS.data.request_body;

import com.ebos.eBOS.data.entity.PersonalMember;
import com.ebos.eBOS.data.entity.User;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;

/**
 * Class representing PersonalMember sign-up request's body
 */
@Data
@Getter
@Setter
public class PMSignUpBody {
    private String username;
    private String phone;
    private String email;
    private String password;
    private String fullName;
    private long dateOfBirth;

    private User toUser() {
        User user = new User();
        user.setEmail(email);
        user.setPhone(phone);
        user.setUsername(username);
        user.setPassword(password);
        return user;
    }


    public PersonalMember toPersonalMember() {
        PersonalMember member = new PersonalMember();
        member.setUser(toUser());
        member.setFullName(fullName);
        member.setDateOfBirth(new Timestamp(dateOfBirth));
        return member;
    }

}
