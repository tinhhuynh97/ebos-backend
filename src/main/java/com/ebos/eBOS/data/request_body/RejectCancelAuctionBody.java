package com.ebos.eBOS.data.request_body;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * This class represents request body for rejecting or canceling auction
 */
@Data
@Getter
@Setter
public class RejectCancelAuctionBody {
    private String reason;
    private String note;
}
