package com.ebos.eBOS.data.request_body;


import com.ebos.eBOS.data.entity.Auction;
import com.ebos.eBOS.data.entity.Category;
import com.ebos.eBOS.data.entity.Item;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Data
@Getter
@Setter
public class SubmitAuctionBody {
    private Integer durationInDay;
    private BigDecimal reservePrice;
    private BigDecimal buyNowPrice;
    private String name;
    private Integer categoryId;
    private String condition;
    private String description;
    @JsonIgnore
    private Category category;

    public Auction toAuction() {
        Auction auction = new Auction();
        auction.setDurationInDay(durationInDay);
        auction.setReservePrice(reservePrice);
        auction.setItem(toItem());
        return auction;
    }

    public Item toItem() {
        Item item = new Item();
        item.setName(name);
        item.setCategory(category);
        item.setCondition(condition);
        item.setDescription(description);
        return item;
    }
}
