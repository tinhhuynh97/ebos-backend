package com.ebos.eBOS.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@AllArgsConstructor
public class AuctionFilterCriteria {
    private String key;
    private String operation;
    private Object value;
}
