package com.ebos.eBOS.data;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@Getter
@Setter
public class ChartResponse {
    private List<String> labels;
    private List<Integer> dataSet;
}
