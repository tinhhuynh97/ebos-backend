package com.ebos.eBOS.data;

import com.ebos.eBOS.data.entity.Auction;
import org.springframework.lang.NonNull;

public class AuctionAffinity implements Comparable<AuctionAffinity> {
    private Auction first;
    private Auction second;
    private Double score;

    public AuctionAffinity() {
    }

    public AuctionAffinity(Auction first, Auction second) {
        this.first = first;
        this.second = second;
    }

    public AuctionAffinity(Auction first, Auction second, Double score) {
        this.first = first;
        this.second = second;
        this.score = score;
    }

    public Auction getFirst() {
        return first;
    }

    public void setFirst(Auction first) {
        this.first = first;
    }

    public Auction getSecond() {
        return second;
    }

    public void setSecond(Auction second) {
        this.second = second;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public int compareTo(@NonNull AuctionAffinity o) {
        return this.score.compareTo(o.score);
    }
}
