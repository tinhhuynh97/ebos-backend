package com.ebos.eBOS.data;

import com.ebos.eBOS.data.entity.Auction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class AuctionSpecification implements Specification<Auction> {

    public static final String OP_GREATER = ">";
    public static final String OP_GREATER_OR_EQUALS = ">=";
    public static final String OP_LESS = "<";
    public static final String OP_LESS_OR_EQUALS = "<=";
    public static final String OP_EQUALS = "=";

    private AuctionFilterCriteria criteria;

    public AuctionSpecification(AuctionFilterCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(@NonNull Root<Auction> root, @NonNull CriteriaQuery<?> criteriaQuery,
                                 @NonNull CriteriaBuilder criteriaBuilder) {
        String operation = criteria.getOperation().toLowerCase();
        switch (operation) {
            case OP_GREATER:
                return criteriaBuilder.greaterThan(
                        root.get(criteria.getKey()), criteria.getValue().toString());
            case OP_GREATER_OR_EQUALS:
                return criteriaBuilder.greaterThanOrEqualTo(
                        root.get(criteria.getKey()), criteria.getValue().toString());
            case OP_LESS:
                return criteriaBuilder.lessThan(
                        root.get(criteria.getKey()), criteria.getValue().toString());
            case OP_LESS_OR_EQUALS:
                return criteriaBuilder.lessThanOrEqualTo(
                        root.get(criteria.getKey()), criteria.getValue().toString());
            case OP_EQUALS:
                return criteriaBuilder.equal(
                        root.get(criteria.getKey()), criteria.getValue().toString());
        }
        return null;
    }

}
