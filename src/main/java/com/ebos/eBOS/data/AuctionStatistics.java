package com.ebos.eBOS.data;


public interface AuctionStatistics {
    String getItemName();

    Integer getCount();
}
