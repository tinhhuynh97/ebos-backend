package com.ebos.eBOS.data;


public interface CategoryStatistics {
    String getCategory();

    int getCount();
}
