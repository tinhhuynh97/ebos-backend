package com.ebos.eBOS.controller;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.entity.FcmToken;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.service.UserService;
import com.ebos.eBOS.service.firebase.FcmService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FcmController extends BaseController {

    private FcmService fcmService;

    public FcmController(FcmService fcmService, UserService userService) {
        super(userService);
        this.fcmService = fcmService;
    }

    @PostMapping("/fcm/addTk")
    public ResponseEntity addToken(@RequestParam("token") String token) {
        if (!StringUtils.isEmpty(token)) {
            User user = getCurrentLoginUser();
            FcmToken fcmToken = new FcmToken();
            fcmToken.setUser(user);
            fcmToken.setValue(token);
            fcmToken.setIsDeleted(false);
            if (!fcmService.checkTokenExists(fcmToken)) {
//                return ResponseEntity.status(HttpStatus.CONFLICT).body("Token is already used");
                fcmService.addToken(fcmToken);
            }
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/fcm/deleteTk")
    public ResponseEntity deleteToken(@RequestParam("token") String token) {
        if (!StringUtils.isEmpty(token)) {
            User user = getCurrentLoginUser();
            FcmToken fcmToken = new FcmToken();
            fcmToken.setUser(user);
            fcmToken.setValue(token);
            fcmToken = fcmService.findTokenByUserAndValue(fcmToken);
            if (fcmToken == null) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body("Token doesn't exist");
            }
            fcmService.delete(fcmToken);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/fcm/ping")
    public ResponseEntity ping() {
        User user = getCurrentLoginUser();
        fcmService.sendMessageToUser(user.getId(), AppConstants.N_TITLE_DEFAULT, "Hello " + user.getUsername(), null);
        return ResponseEntity.ok().build();
    }

}
