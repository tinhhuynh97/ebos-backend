package com.ebos.eBOS.controller;

import com.ebos.eBOS.data.Views;
import com.ebos.eBOS.data.entity.Address;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.exception.InvalidRequestException;
import com.ebos.eBOS.exception.ResourceNotFoundException;
import com.ebos.eBOS.service.AddressService;
import com.ebos.eBOS.service.UserService;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class
AddressController extends BaseController {

    private AddressService addressService;

    public AddressController(AddressService addressService, UserService userService) {
        super(userService);

        this.addressService = addressService;
    }


    @PostMapping("/addresses/personal/add")
    public ResponseEntity addPersonalAddress(@RequestBody Address address) {
        User user = getCurrentLoginUser();
        address.setOwner(user);
        addressService.insertPersonalAddress(address);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/addresses/personal/checkHavingAny")
    public ResponseEntity checkHavingAnyAddress() {
        User user = getCurrentLoginUser();

        return ResponseEntity.status(HttpStatus.OK)
                .body(addressService.checkHavingAnyAddress(user));
    }

    @GetMapping("/contact/sellerOfWonItem")
    @JsonView(Views.ContactInfo.class)
    public ResponseEntity getSellerContactInfoOfWonItem(@RequestParam("auctionId") Integer auctionId) {
        User user = getCurrentLoginUser();
        try {
            return ResponseEntity.ok(addressService.getSellerContactInfoOfWonItem(user, auctionId));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (InvalidRequestException e) {
            return ResponseEntity.unprocessableEntity().body(e.getMessage());
        }
    }

    @GetMapping("/contact/itemWinner")
    @JsonView(Views.ContactInfo.class)
    public ResponseEntity getItemWinnerContactInfo(@RequestParam("auctionId") Integer auctionId) {
        User user = getCurrentLoginUser();
        try {
            return ResponseEntity.ok(addressService.getItemWinnerContactInfo(user, auctionId));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (InvalidRequestException e) {
            return ResponseEntity.unprocessableEntity().body(e.getMessage());
        }
    }


}
