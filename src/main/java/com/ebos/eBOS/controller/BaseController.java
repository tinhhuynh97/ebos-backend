package com.ebos.eBOS.controller;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.service.UserService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RestController;

@RestController
public abstract class BaseController {

    protected UserService userService;

    public BaseController(UserService userService) {
        this.userService = userService;
    }

    protected User getCurrentLoginUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String phone = (String) auth.getPrincipal();
        return userService.findByPhone(phone);
    }

    protected Pageable createPageRequest(Integer page, Integer size) {
        if (page >= 0 && size >= 1)
            return PageRequest.of(page, size);
        return null;
    }

    protected Pageable createPageRequest(Integer page, Integer size, Sort sort) {
        if (page >= 0 && size >= 1)
            return PageRequest.of(page, size, sort);
        return null;
    }

    protected Pageable setUpPagable(int page, int size, String sort, String dir) {
        Pageable pageable = null;
        if (page >= 0 && size >= 1) {
            if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(dir)) {
                Sort srt = null;
                switch (sort) {
                    case AppConstants.SORT_TIME:
                        if (dir.equals(AppConstants.DIR_ENDING_SOON)) {
                            srt = new Sort(Sort.Direction.ASC, "plannedEndTime");
                        } else if (dir.equals(AppConstants.DIR_NEWLY_LISTED)) {
                            srt = new Sort(Sort.Direction.DESC, "startTime");
                        }
                        break;
                    case AppConstants.SORT_CURRENT_PRICE:
                        if (dir.equals(AppConstants.DIR_ASC)) {
                            srt = new Sort(Sort.Direction.ASC, "currentPrice");
                        } else if (dir.equals(AppConstants.DIR_DESC)) {
                            srt = new Sort(Sort.Direction.DESC, "currentPrice");
                        }
                        break;
                    case AppConstants.SORT_NUM_OF_BID:
                        if (dir.equals(AppConstants.DIR_ASC)) {
                            srt = new Sort(Sort.Direction.ASC, "numberOfBids");
                        } else if (dir.equals(AppConstants.DIR_DESC)) {
                            srt = new Sort(Sort.Direction.DESC, "numberOfBids");
                        }
                        break;
                    case AppConstants.SORT_ENDED_TIME:
                        if (dir.equals(AppConstants.DIR_ASC)) {
                            srt = new Sort(Sort.Direction.ASC, "actualEndTime");
                        } else if (dir.equals(AppConstants.DIR_DESC)) {
                            srt = new Sort(Sort.Direction.DESC, "actualEndTime");
                        }
                        break;

                    case AppConstants.SORT_TIME_LEFT:
                        if (dir.equals(AppConstants.DIR_ASC)) {
                            srt = new Sort(Sort.Direction.ASC, "plannedEndTime");
                        } else if (dir.equals(AppConstants.DIR_DESC)) {
                            srt = new Sort(Sort.Direction.DESC, "plannedEndTime");
                        }
                        break;
                }
                pageable = createPageRequest(page, size, srt);
            } else if (StringUtils.isEmpty(sort) && StringUtils.isEmpty(dir)) {
                pageable = createPageRequest(page, size);
            }
        }
        return pageable;
    }
}
