package com.ebos.eBOS.controller;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.AuctionFilterCriteria;
import com.ebos.eBOS.data.AuctionSpecification;
import com.ebos.eBOS.data.ChartResponse;
import com.ebos.eBOS.data.Views;
import com.ebos.eBOS.data.entity.Auction;
import com.ebos.eBOS.data.entity.Category;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.data.request_body.RejectCancelAuctionBody;
import com.ebos.eBOS.data.request_body.SubmitAuctionBody;
import com.ebos.eBOS.exception.AuctionNotFoundException;
import com.ebos.eBOS.exception.InvalidRequestException;
import com.ebos.eBOS.service.AuctionService;
import com.ebos.eBOS.service.CategoryService;
import com.ebos.eBOS.service.UserService;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@RestController
public class AuctionController extends BaseController {

    private AuctionService auctionService;
    private CategoryService categoryService;

    public AuctionController(AuctionService auctionService,
                             CategoryService categoryService,
                             UserService userService) {
        super(userService);
        this.auctionService = auctionService;
        this.categoryService = categoryService;
    }

    @PostMapping(value = "/auction/add", consumes = {"multipart/form-data"})
    public ResponseEntity addAuction(@RequestParam("auction") String body,
                                     @RequestParam("images") List<MultipartFile> images) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SubmitAuctionBody submitAuctionBody = mapper.readValue(body, SubmitAuctionBody.class);

        Category category = categoryService.getById(submitAuctionBody.getCategoryId());
        if (category == null) {
            return ResponseEntity.badRequest().build();
        }

        User user = getCurrentLoginUser();
        Auction auction = submitAuctionBody.toAuction();
        auction.getItem().setSeller(user);
        auction.getItem().setCategory(category);
        try {
            auctionService.add(auction, images);
            return ResponseEntity.ok().build();
        } catch (InvalidRequestException e) {
            return ResponseEntity.unprocessableEntity().body(e.getMessage());
        }

    }

    @GetMapping(value = "/auction/details/{id}")
    @JsonView(Views.Details.class)
    public ResponseEntity<Auction> getActiveAuctionDetailsById(@PathVariable("id") Integer id) {
        User user = getCurrentLoginUser();
        Auction auction = null;
        if (user == null) {
            auction = auctionService.getActiveAuctionById(id);
        } else {
            auction = auctionService.getActiveAuctionByIdForUser(id, user);
        }
        if (auction != null) {
            return ResponseEntity.ok(auction);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }


    @GetMapping(value = "/auction/search", params = {"categoryId"})
    @JsonView(Views.Simple.class)
    public ResponseEntity<Page<Auction>> getActiveAuctionsbyCategory(@RequestParam("categoryId") Integer categoryId,
                                                                     @RequestParam("page") Integer page,
                                                                     @RequestParam("size") Integer size,
                                                                     @RequestParam(value = "sort", required = false) String sort,
                                                                     @RequestParam(value = "dir", required = false) String dir,
                                                                     @RequestParam(value = "minPrice", required = false) BigDecimal minPrice,
                                                                     @RequestParam(value = "maxPrice", required = false) BigDecimal maxPrice) {


        Pageable pageable = setUpPagable(page, size, sort, dir);
        if (pageable != null) {
            return ResponseEntity.ok(auctionService.getActiveAuctionsByCategory(categoryId, minPrice, maxPrice, pageable));
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping(value = "/auction/search", params = {"q"})
    @JsonView(Views.Simple.class)
    public ResponseEntity<Page<Auction>> getActiveAuctionsbyKeyword(@RequestParam("q") String keyword,
                                                                    @RequestParam("page") Integer page,
                                                                    @RequestParam("size") Integer size,
                                                                    @RequestParam(value = "sort", required = false) String sort,
                                                                    @RequestParam(value = "dir", required = false) String dir,
                                                                    @RequestParam(value = "minPrice", required = false) BigDecimal minPrice,
                                                                    @RequestParam(value = "maxPrice", required = false) BigDecimal maxPrice) {
        Pageable pageable = setUpPagable(page, size, sort, dir);
        if (pageable != null) {
            return ResponseEntity.ok(auctionService.getActiveAuctionsByKeyword(keyword, minPrice, maxPrice, pageable));
        }
        return ResponseEntity.badRequest().build();
    }

    private Specification<Auction> setUpPriceRangeSpec(BigDecimal minPrice, BigDecimal maxPrice) {
        if (minPrice == null && maxPrice == null) {
            return null;
        }
        AuctionSpecification minPriceSpec =
                new AuctionSpecification(new AuctionFilterCriteria("currentPrice",
                        AuctionSpecification.OP_GREATER_OR_EQUALS, minPrice));

        AuctionSpecification maxPriceSpec =
                new AuctionSpecification(new AuctionFilterCriteria("currentPrice",
                        AuctionSpecification.OP_LESS_OR_EQUALS, maxPrice));

        return Specification.where(minPriceSpec).and(maxPriceSpec);
    }

    @GetMapping(value = "/auction/all")
    @JsonView(Views.Simple.class)
    public ResponseEntity getActiveAuctionsSort(@RequestParam("page") Integer page,
                                                @RequestParam("size") Integer size,
                                                @RequestParam(value = "sort", required = false) String sort,
                                                @RequestParam(value = "dir", required = false) String dir) {
        Pageable pageable = setUpPagable(page, size, sort, dir);
        if (pageable != null) {
            return ResponseEntity.ok(auctionService.getAllActiveAuctions(pageable));
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/auction/getSuggestedBidPrice")
    public ResponseEntity<List<BigDecimal>> getSuggestedBidPrice(@RequestParam("auctionId") Integer auctionId) {
        try {
            List<BigDecimal> prices = auctionService.getSuggestedBidPrice(auctionId);
            return ResponseEntity.ok(prices);
        } catch (AuctionNotFoundException e) {
            return ResponseEntity.unprocessableEntity().build();
        }
    }

    @GetMapping("/auction/history/sell/{status}")
    @JsonView(Views.AuctioneerHistory.class)
    public ResponseEntity<Page<Auction>> getSellingHistoryAuctions(@RequestParam("page") Integer page,
                                                                   @RequestParam("size") Integer size,
                                                                   @PathVariable("status") String status,
                                                                   @RequestParam(value = "sort", required = false) String sort,
                                                                   @RequestParam(value = "dir", required = false) String dir) {
        User seller = getCurrentLoginUser();
        Pageable pageable = setUpPagable(page, size, sort, dir);
        if (pageable != null) {
            switch (status) {
                case AppConstants.HISTORY_STT_REJECTED_CANCELLED:
                    return ResponseEntity.ok(auctionService.getRejectedOrCanceledAuctionsBySeller(seller, pageable));
                case AppConstants.HISTORY_STT_SELLING:
                    return ResponseEntity.ok(auctionService.getSellingAuctionsBySeller(seller, pageable));
                case AppConstants.HISTORY_STT_ENDED:
                    return ResponseEntity.ok(auctionService.getEndedAuctionsBySeller(seller, pageable));
                case AppConstants.HISTORY_STT_PENDING:
                    return ResponseEntity.ok(auctionService.getPendingAuctionsBySeller(seller, pageable));
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/auction/history/bid/{status}")
    @JsonView(Views.BidderHistory.class)
    public ResponseEntity<Page<Auction>> getBuyingHistoryAuctions(@RequestParam("page") Integer page,
                                                                  @RequestParam("size") Integer size,
                                                                  @PathVariable("status") String status,
                                                                  @RequestParam(value = "sort", required = false) String sort,
                                                                  @RequestParam(value = "dir", required = false) String dir) {

        User bidder = getCurrentLoginUser();

        Pageable pageable = setUpPagable(page, size, sort, dir);
        switch (status) {
            case AppConstants.HISTORY_STT_LOST:
                return ResponseEntity.ok(auctionService.getLostAuctionsByBidder(bidder, pageable));
            case AppConstants.HISTORY_STT_WON:
                return ResponseEntity.ok(auctionService.getWonAuctionsByBidder(bidder, pageable));
            case AppConstants.HISTORY_STT_BIDDING:
                return ResponseEntity.ok(auctionService.getBiddingAuctionsByBidder(bidder, pageable));
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/auction/browse")
    public ResponseEntity saveBrowseHistory(@RequestParam("auctionId") Integer auctionId) {
        User user = getCurrentLoginUser();
        try {
            auctionService.saveBrowseHistory(user, auctionId);
            return ResponseEntity.ok().build();
        } catch (AuctionNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/auction/recommended")
    @JsonView(Views.Simple.class)
    public ResponseEntity getRecommendedAuctions(@RequestParam("auctionId") Integer auctionId) {
        try {
            User user = getCurrentLoginUser();
            return ResponseEntity.ok(auctionService.getRecommendedAuctions(user, auctionId));
        } catch (AuctionNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/auction/recentlyViewed")
    @JsonView(Views.Simple.class)
    public ResponseEntity getRecentlyViewedAuctions() {
        User user = getCurrentLoginUser();
        return ResponseEntity.ok(auctionService.getRecentlyViewedAuctions(user));
    }


    @GetMapping("/admin/pendingAuctions")
    @JsonView(Views.Details.class)
    public ResponseEntity<Page<Auction>> getPendingAuctions(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        Pageable pageable = createPageRequest(page, size);
        if (page >= 0 && size >= 1) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(auctionService.getPendingAuctions(pageable));
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/admin/acceptAuction/{id}")
    public ResponseEntity acceptAuction(@PathVariable("id") Integer id) {
        User admin = getCurrentLoginUser();
        try {
            auctionService.acceptAuctions(id, admin);
        } catch (InvalidRequestException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/admin/refuseAuction/{id}")
    public ResponseEntity refuseAuction(@PathVariable("id") Integer id, @RequestBody RejectCancelAuctionBody body) {
        User admin = getCurrentLoginUser();
        try {
            auctionService.rejectAuction(id, body.getReason(), body.getNote(), admin);
        } catch (InvalidRequestException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/admin/countAuctions/{status}")
    public ResponseEntity countAuctionsByStatus(@PathVariable("status") String status) {
        return ResponseEntity.ok(auctionService.getCountAllAuctionsByStatus(status));
    }

    @GetMapping("/admin/chart/countStartedAuctions/{timeMode}")
    public ResponseEntity countSubmittedAuctions(@PathVariable("timeMode") String timeMode) {
        ChartResponse response;
        switch (timeMode) {
            case AppConstants.TIME_MODE_DAILY:
                response = auctionService.getDailyCountStartedByChart();
                break;
            case AppConstants.TIME_MODE_WEEKLY:
                response = auctionService.getWeeklyCountStartByChart();
                break;
            case AppConstants.TIME_MODE_MONTHLY:
                response = auctionService.getMonthlyCountStartByChart();
                break;
            default:
                return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/admin/top5AuctionsHaveMostBids")
    public ResponseEntity getTop5AuctionsHaveMostBids() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(auctionService.getTop5AuctionsHaveMostBids());
    }

    @GetMapping("/admin/auctions/{status}")
    @JsonView(Views.Details.class)
    public ResponseEntity<Page<Auction>> getAuctions(@PathVariable("status") String status,
                                                     @RequestParam("page") Integer page,
                                                     @RequestParam("size") Integer size,
                                                     @RequestParam(value = "sort", required = false) String sort,
                                                     @RequestParam(value = "dir", required = false) String dir) {
        Pageable pageable = setUpPagable(page, size, sort, dir);
        if (pageable != null) {
            return ResponseEntity.ok(auctionService.getAuctionByStatus(status, pageable));
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/admin/auctions/{status}/search")
    @JsonView(Views.Details.class)
    public ResponseEntity<Page<Auction>> getAuctionsByNameAndStatus(@PathVariable("status") String status,
                                                                    @RequestParam("name") String name,
                                                                    @RequestParam("page") Integer page,
                                                                    @RequestParam("size") Integer size,
                                                                    @RequestParam(value = "sort", required = false) String sort,
                                                                    @RequestParam(value = "dir", required = false) String dir) {
        Pageable pageable = setUpPagable(page, size, sort, dir);
        if (pageable != null) {
            return ResponseEntity.ok(auctionService.getAuctionByNameAndStatus(name, status, pageable));
        }
        return ResponseEntity.badRequest().build();
    }


}

