package com.ebos.eBOS.controller;

import com.ebos.eBOS.service.MediaService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MediaController {

    private MediaService mediaService;

    public MediaController(MediaService mediaService) {
        this.mediaService = mediaService;
    }
//
//    @GetMapping("/files/{fileName:.+}")
//    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) throws IOException {
//        // Load file as Resource
//        Resource resource = mediaService.loadFileAsResource(fileName);
//
//        // Try to determine file's content type
//        String contentType = null;
//        contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
//
//        // Fallback to the default content type if type could not be determined
//        if (contentType == null) {
//            contentType = "application/octet-stream";
//        }
//
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(contentType))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
//                .body(resource);
//    }

}
