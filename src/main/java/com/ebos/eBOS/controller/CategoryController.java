package com.ebos.eBOS.controller;

import com.ebos.eBOS.data.entity.Category;
import com.ebos.eBOS.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CategoryController {

    private CategoryService service;

    public CategoryController(CategoryService service) {
        this.service = service;
    }

    @GetMapping("/categories")
    ResponseEntity<List<Category>> getCategories() {
        return ResponseEntity.status(HttpStatus.OK).body(service.getCategories());
    }

    @GetMapping("/admin/top5CategoriesHaveMostItems")
    public ResponseEntity getTop5CategoriesHaveMostItems() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getTop5CategoriesHaveMostItems());
    }
}
