package com.ebos.eBOS.controller;

import com.ebos.eBOS.AppConstants;
import com.ebos.eBOS.data.ChartResponse;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.exception.AuctionNotFoundException;
import com.ebos.eBOS.exception.InvalidRequestException;
import com.ebos.eBOS.service.BidService;
import com.ebos.eBOS.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
public class BidController extends BaseController {

    private BidService bidService;

    public BidController(BidService bidService, UserService userService) {
        super(userService);
        this.bidService = bidService;
    }

    @PostMapping("/bid/add")
    public ResponseEntity addBid(@RequestParam("auctionId") Integer auctionId, @RequestParam("price") BigDecimal price) {
        User bidder = getCurrentLoginUser();
        try {
            bidService.addBid(auctionId, bidder, price);
            return ResponseEntity.ok().build();
        } catch (AuctionNotFoundException | InvalidRequestException e) {
            return ResponseEntity.unprocessableEntity().body(e.getMessage());
        }
    }

    @GetMapping("/admin/chart/countBids/{timeMode}")
    public ResponseEntity countBids(@PathVariable("timeMode") String timeMode) {
        ChartResponse response;
        switch (timeMode) {
            case AppConstants.TIME_MODE_DAILY:
                response = bidService.getDailyCountBidsByChart();
                break;
            case AppConstants.TIME_MODE_WEEKLY:
                response = bidService.getWeeklyCountBidsByChart();
                break;
            case AppConstants.TIME_MODE_MONTHLY:
                response = bidService.getMonthlyCountBidsByChart();
                break;
            default:
                return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(response);
    }

}
