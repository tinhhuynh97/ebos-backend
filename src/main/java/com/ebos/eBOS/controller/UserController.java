package com.ebos.eBOS.controller;

import com.ebos.eBOS.data.Views;
import com.ebos.eBOS.data.entity.PersonalMember;
import com.ebos.eBOS.data.entity.User;
import com.ebos.eBOS.data.request_body.PMSignUpBody;
import com.ebos.eBOS.exception.ResourceNotFoundException;
import com.ebos.eBOS.service.UserService;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController extends BaseController {
    private UserService service;

    public UserController(UserService service) {
        super(service);
        this.service = service;
    }

    @PostMapping("/users/personal/sign-up")
    public ResponseEntity personalSignUp(@RequestBody PMSignUpBody body) {
        PersonalMember member = body.toPersonalMember();
        User user = member.getUser();

        if (service.checkExistingPhone(user.getPhone())) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Phone is already used");
        }
        if (service.checkExistingEmail(user.getEmail())) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Email is already used");
        }
        if (service.checkExistingUsername(user.getUsername())) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Username is already used");
        }

        service.personalSignUp(member);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/users/personal")
    @JsonView(Views.PublicInfo.class)
    public ResponseEntity<Page<PersonalMember>> getPersonalMembers(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        if (page < 0 || size < 1) {
            return ResponseEntity.badRequest().build();
        }

        Pageable pageRequest = createPageRequest(page, size);
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getPersonalMembers(pageRequest));
    }

    @GetMapping("/users/personal/ownInfo")
    @JsonView(Views.PrivateInfo.class)
    public ResponseEntity<PersonalMember> getPersonalMember() {
        User user = getCurrentLoginUser();
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getPersonalMemberById(user.getId()));
    }

    @GetMapping("/admin/top5SellersHavingMostAuctions")
    public ResponseEntity getTop5SellersHavingMostAuctions() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getTop5SellersHavingMostAuctions());
    }

    @GetMapping("/admin/top5BiddersHaveMostWonItems")
    public ResponseEntity getTop5BiddersHaveMostWonItems() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getTop5BiddersHaveMostWonItems());
    }

    @GetMapping("/admin/personalMembers/{status}")
    @JsonView(Views.PrivateInfo.class)
    public ResponseEntity getpersonalMembersByStatus(@RequestParam("page") Integer page,
                                                     @RequestParam("size") Integer size,
                                                     @PathVariable("status") String status) {
        Pageable pageable = createPageRequest(page, size);
        if (pageable != null) {
            return ResponseEntity.ok(userService.getPersonalMemberByStatus(status, pageable));
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/admin/personalMembers/activate")
    public ResponseEntity activePersonalMember(@RequestParam("id") Integer id) {
        User admin = getCurrentLoginUser();
        try {
            userService.activatePersonalMember(id, admin);
            return ResponseEntity.ok().build();
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PostMapping("/admin/personalMembers/restrict")
    public ResponseEntity restrictPersonalMember(@RequestParam("id") Integer id, @RequestParam("note") String note) {
        User admin = getCurrentLoginUser();
        try {
            userService.restrictPersonalMember(id, note, admin);
            return ResponseEntity.ok().build();
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/admin/personalMembers/{status}/count")
    public ResponseEntity countPersonalMembersByStatus(@PathVariable("status") String status) {
        return ResponseEntity.ok(userService.countPersonalMembersByStatus(status));
    }

    @GetMapping("/admin/personalMembers/{status}/search")
    @JsonView(Views.PrivateInfo.class)
    public ResponseEntity getPersonalMembersByKeyword(@PathVariable("status") String status,
                                                      @RequestParam("q") String keyword,
                                                      @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                      @RequestParam(value = "size", defaultValue = "10") Integer size) {
        Pageable pageable = createPageRequest(page, size);
        if (pageable != null) {
            return ResponseEntity.ok(userService.getPersonalMembersByKeyword(keyword, status, pageable));
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("users/status")
    public ResponseEntity getUserStatus() {
        User user = getCurrentLoginUser();
        return ResponseEntity.ok().body(user.getStatus());
    }



}
