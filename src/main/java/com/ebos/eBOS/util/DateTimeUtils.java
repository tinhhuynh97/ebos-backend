package com.ebos.eBOS.util;

import java.time.*;
import java.time.temporal.ChronoUnit;

public class DateTimeUtils {

    public static Long calculateTimeLeft(ZonedDateTime dateTime, int durationInDays) {
        dateTime = dateTime.plusDays(durationInDays);
        ZonedDateTime now = ZonedDateTime.now();
        ChronoUnit unit = ChronoUnit.MILLIS;
        return unit.between(now, dateTime);
    }

    public static Long calculateTimeLeft(ZonedDateTime dateTime) {
        ZonedDateTime now = ZonedDateTime.now();
        ChronoUnit unit = ChronoUnit.MILLIS;
        return unit.between(now, dateTime);
    }

    public static ZonedDateTime getStartOfToDay() {
        LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
        return today.atZone((ZoneId.of("Asia/Saigon")));
    }

    public static ZonedDateTime getEndOfToDay() {
        LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
        return today.atZone((ZoneId.of("Asia/Saigon")));
    }

    public static ZonedDateTime getStartOfDateTime(ZonedDateTime dateTime) {
        LocalDate date = dateTime.toLocalDate();
        LocalDateTime temp = LocalDateTime.of(date, LocalTime.MIN);
        return temp.atZone((ZoneId.of("Asia/Saigon")));
    }

    public static ZonedDateTime getEndOfDateTime(ZonedDateTime dateTime) {
        LocalDate date = dateTime.toLocalDate();
        LocalDateTime temp = LocalDateTime.of(date, LocalTime.MAX);
        return temp.atZone((ZoneId.of("Asia/Saigon")));
    }
}
