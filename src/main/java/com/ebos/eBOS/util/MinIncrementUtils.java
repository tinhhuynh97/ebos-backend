package com.ebos.eBOS.util;

import com.ebos.eBOS.data.entity.MinIncrementRule;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Data
@Getter
@Setter
public class MinIncrementUtils {

    private static List<MinIncrementRule> minIncrementRuleList;

    public static void setUpRules(List<MinIncrementRule> rules) {
        minIncrementRuleList = rules;
    }

    public static BigDecimal calculateIncrement(BigDecimal input) {
        BigDecimal increment = null;
        if (!CollectionUtils.isEmpty(minIncrementRuleList)) {
            MinIncrementRule rule = minIncrementRuleList.stream()
                    .filter(minIncrementRule -> {
                        if (minIncrementRule.getMaxPrice() != null) {
                            return input.compareTo(minIncrementRule.getMinPrice()) >= 0
                                    && input.compareTo(minIncrementRule.getMaxPrice()) <= 0;
                        } else {
                            return input.compareTo(minIncrementRule.getMinPrice()) >= 0;
                        }
                    })
                    .findFirst()
                    .orElse(null);
            if (rule != null) {
                increment = rule.getIncrement();
            }
        }
        return increment;
    }

    public static BigDecimal addIncrement(BigDecimal price) {
        BigDecimal minBid = null;
        BigDecimal increment = calculateIncrement(price);
        if (increment != null) {
            minBid = price.add(increment);
        }
        return minBid;
    }

}
