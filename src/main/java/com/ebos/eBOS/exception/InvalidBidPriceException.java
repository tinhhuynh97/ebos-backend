package com.ebos.eBOS.exception;

import com.ebos.eBOS.AppConstants;

public class InvalidBidPriceException extends InvalidRequestException {

    public InvalidBidPriceException() {
        super(AppConstants.EXCEPTION_BID_LOWER_THAN_MINIMUM_BID);
    }

    public InvalidBidPriceException(String message) {
        super(message);
    }

    public InvalidBidPriceException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidBidPriceException(Throwable cause) {
        super(cause);
    }

    public InvalidBidPriceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
