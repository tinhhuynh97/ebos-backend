package com.ebos.eBOS.exception;

import com.ebos.eBOS.AppConstants;

public class AuctionNotFoundException extends ResourceNotFoundException {

    public AuctionNotFoundException() {
        super(AppConstants.EXCEPTION_NO_AUCTION_EXISTS);
    }

    public AuctionNotFoundException(String message) {
        super(message);
    }

    public AuctionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuctionNotFoundException(Throwable cause) {
        super(cause);
    }

    public AuctionNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
