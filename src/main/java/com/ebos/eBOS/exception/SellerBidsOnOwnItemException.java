package com.ebos.eBOS.exception;

import com.ebos.eBOS.AppConstants;

public class SellerBidsOnOwnItemException extends InvalidRequestException {

    public SellerBidsOnOwnItemException() {
        super(AppConstants.EXCEPTION_SELLER_BIDS_ON_OWN_ITEM);
    }

    public SellerBidsOnOwnItemException(String message) {
        super(message);
    }

    public SellerBidsOnOwnItemException(String message, Throwable cause) {
        super(message, cause);
    }

    public SellerBidsOnOwnItemException(Throwable cause) {
        super(cause);
    }

    public SellerBidsOnOwnItemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
