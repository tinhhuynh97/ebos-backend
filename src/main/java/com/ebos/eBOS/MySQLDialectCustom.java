package com.ebos.eBOS;

import org.hibernate.dialect.MySQL57Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

public class MySQLDialectCustom extends MySQL57Dialect {

    public MySQLDialectCustom() {
        super();
        registerFunction("MATCH", new SQLFunctionTemplate(StandardBasicTypes.DOUBLE,
                "MATCH(?1) AGAINST (?2 IN BOOLEAN MODE)"));
    }
}

