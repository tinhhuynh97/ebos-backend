package com.ebos.eBOS.security;

import com.ebos.eBOS.data.entity.Role;
import com.ebos.eBOS.repository.RoleRepository;
import com.ebos.eBOS.repository.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserDetailsServiceImpl(UserRepository userRepository,
                                  RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String phone) throws PhoneNotFoundException {
        com.ebos.eBOS.data.entity.User user = userRepository.findByPhone(phone);

        if (user == null) {
            throw new PhoneNotFoundException(phone);
        }

        List<Role> roles = roleRepository.findByUserId(user.getId());

        Collection<GrantedAuthority> authorities = roles.stream().map(role ->
                (GrantedAuthority) role::getName).collect(Collectors.toList());

        return new User(user.getPhone(),
                user.getPassword(),
                authorities);
    }
}
