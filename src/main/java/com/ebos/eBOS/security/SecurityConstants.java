package com.ebos.eBOS.security;

public class SecurityConstants {

    public static final String JWT_SECRET = "JWTSecretKey";
    public static final String AUTHORITIES_KEY = "JWTAuthoritiesKey";
    public static final long EXPIRATION_TIME = 864_000_000 * 2; // 20 days

    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String HEADER_ACCESS_CONTROLL_EXPOSE = "Access-Control-Expose-Headers";

    public static final String SIGN_UP_URL = "/users/**/sign-up";

}
