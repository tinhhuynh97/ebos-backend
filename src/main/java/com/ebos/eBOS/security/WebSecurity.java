package com.ebos.eBOS.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.ebos.eBOS.security.SecurityConstants.SIGN_UP_URL;


@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceImpl userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurity(UserDetailsServiceImpl userDetailsService,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
                .antMatchers("/categories",
                        "/auction/search",
                        "/users/personal",
                        "/files/**",
                        "/auction/details/**",
                        "/auction/all",
                        "/auction/getSuggestedBidPrice",
                        "/auction/recommended").permitAll()
                .antMatchers("/addresses/personal/**",
                        "/auction/add",
                        "/users/personal/info",
                        "/fcm/addTk",
                        "/fcm/ping",
                        "/users/status",
                        "/auction/history/sell/**",
                        "/contact/itemWinner").hasAuthority("AUCTIONEER")
                .antMatchers("/addresses/personal/**",
                        "/users/personal/info",
                        "/fcm/addTk",
                        "/fcm/ping",
                        "/bid/add",
                        "/users/status",
                        "/auction/history/bid/**",
                        "/contact/sellerOfWonItem",
                        "/auction/browse",
                        "/auction/recentlyViewed").hasAuthority("BIDDER")
                .antMatchers("/admin/**",
                        "/fcm/addToken").hasAuthority("ADMIN")
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                // disable session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }


}
